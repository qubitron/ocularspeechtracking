#%% import packages
import mne
import numpy as np
import glob
import eelbrain as eb
from pymatreader import read_mat
from os import mkdir, listdir
from os.path import join, isdir 

import joblib
from scipy.stats import spearmanr, zscore
from itertools import compress

from scipy.io import savemat
from sklearn.utils import shuffle
from copy import deepcopy


#%%
raw_files = listdir('/mnt/obob/staff/jschubert/experiments/cocktail_eye/data/meg/cocktail_preproc_contin')
data_dir = '/mnt/obob/staff/qgehmacher/git/ocular_speech_tracking/data/aligned/meg_eye'
all_files = sorted(listdir(data_dir))
all_subjects = [f[:12] for f in all_files]
all_subjects = sorted(all_subjects)

#%%
for subject in all_subjects:
        
        data = read_mat(join(data_dir, subject+'.mat'))
        sub_data = data['data_all']
        
        conds = [2, 3, 4]
        
        #Get Info & build sensor map
        raw_dir = '/mnt/sinuhe/data_raw/gr_cocktail_effort/subject_subject'
        fname = glob.glob(join(raw_dir, '**/%s_cocktail_effort_block01_trans_sss.fif' % subject),
                recursive = True)[0] # returns a list of all matching paths, take the first (and only)
        info = mne.io.read_info(fname, verbose=False)
        info = mne.pick_info(info, mne.pick_types(info, meg='mag'))
        
         # work around eb error...
        otherdir = '/mnt/obob/staff/jschubert/experiments/cocktail_eye/data/mediation_analysis/boosting_combined_lasso'
        data_eb = joblib.load(join(otherdir, '19891222gbhl' + '.dat'))
        sensor = data_eb['fit_simple']['ass']['r_rank'].sensor

        def gen_ndvars(subject_data, cond, feature, feature_type='single', feature_info=None):
                labels = subject_data['label']
                if feature == 'mags':
                        feature_mask = [index for index, label in enumerate(labels) if label.startswith("MEG") and label.endswith("1")]
                elif feature == 'grads':
                        feature_mask = [index for index, label in enumerate(labels) if label.startswith("MEG") and label.endswith("1")]
                else:
                        feature_mask = [lbl.__contains__(feature) for lbl in labels]
                        
                trl_info = subject_data['trialinfo']

                # select trial according to condition
                trial = list(compress(subject_data['trial'],trl_info[:,0] == cond))
                trial = [array[feature_mask] for array in trial]

                # aplly zscoring...
                def zscore_across_stim(array):
                        carray = np.hstack(array)
                        mu = np.mean(carray)
                        std = np.std(carray)
                        c_scaled = (carray - mu) / std
                        min = np.min(c_scaled)
                        # Apply Z-score normalization to each array
                        scaled = [((arr - mu) / std) + min*-1 for arr in array]
                        return scaled
                
                def zscore_across(array):
                        carray = np.hstack(array)
                        mu = np.mean(carray)
                        std = np.std(carray)
                        # Apply Z-score normalization to each array
                        scaled = [(arr - mu) / std for arr in array]
                        return scaled
                
                if feature == 'envelope':
                        trial = zscore_across_stim(trial)       
                else:
                        trial = zscore_across(trial)
                        
                zero_pad = np.zeros((np.shape(trial[0])[0],int(subject_data['fsample']*0.7)))
                trial = [np.hstack((trl, zero_pad)) for trl in trial]
                trial = np.concatenate(trial,axis = 1)
                # get useful time axis (after we append all trials to same cond.)
                n_samples = trial.shape[1]
                time_info = eb.UTS(0, 1/subject_data['fsample'], n_samples)

                # select only feature of interest       
                if feature_type == 'sensors':
                        return eb.NDVar(trial, (sensor, time_info), name=feature)
                elif feature_type == 'single':
                        return eb.NDVar(trial[0], (time_info,), name=feature)
                elif feature_type == 'fake':
                        #fake sensors
                        fakesensor = eb.Scalar(name='fake_sensors', values=np.arange(sum(feature_mask)))
                        return eb.NDVar(trial, (fakesensor, time_info), name=feature)

        
        input_data = [gen_ndvars(sub_data, cond, 'envelope') for cond in conds]
        resp_data = [gen_ndvars(sub_data, cond, 'mags', feature_type='sensors') for cond in conds]
        med_data_x = [gen_ndvars(sub_data, cond, 'x', feature_type='fake') for cond in conds]
        med_data_y = [gen_ndvars(sub_data, cond, 'y', feature_type='fake') for cond in conds]

        
        def shuffle_time(input):
                        input.x = shuffle(input.x.T, random_state=ord(subject[-4])).T
                        return input
                
        ctrl_data_x = deepcopy(med_data_x)
        ctrl_data_x = [shuffle_time(ctrl_data_x[cond]) for cond in np.arange(len(conds))]
        ctrl_data_y = deepcopy(med_data_y)
        ctrl_data_y = [shuffle_time(ctrl_data_y[cond]) for cond in np.arange(len(conds))]
        
        #% Compute encoding/ decoding models
        def do_boosting(input, response, trf_win = [-0.1, 0.550], b_win=0.05):
                
                model = eb.boosting(response, input, 
                        tstart=trf_win[0], tstop=trf_win[1], basis=b_win, scale_data=False, 
                        partitions=4, error='l2', test=True, selective_stopping=False)
                
                return model.r_rank, model.h, model.h_time
        
        
        # simple model
        simple_ass, simple_ams, simple_ums = [do_boosting(input_data[cond], resp_data[cond]) 
                                                for cond in np.arange(len(conds))]
        s_ass_corr, s_ass_trf, s_ass_time, = zip(simple_ass)
        s_ams_corr, s_ams_trf, s_ams_time, = zip(simple_ams)
        s_ums_corr, s_ums_trf, s_ums_time, = zip(simple_ums)

        # mediation model
        med_ass_x, med_ams_x, med_ums_x = [do_boosting([input_data[cond],med_data_x[cond]], resp_data[cond]) 
                                                for cond in np.arange(len(conds))]
        
        m_ass_corr_x, m_ass_trf_x, m_ass_time_x, = zip(med_ass_x)
        m_ams_corr_x, m_ams_trf_x, m_ams_time_x, = zip(med_ams_x)
        m_ums_corr_x, m_ums_trf_x, m_ums_time_x, = zip(med_ums_x)
        
        med_ass_y, med_ams_y, med_ums_y = [do_boosting([input_data[cond],med_data_y[cond]], resp_data[cond]) 
                                                for cond in np.arange(len(conds))]
        
        m_ass_corr_y, m_ass_trf_y, m_ass_time_y, = zip(med_ass_y)
        m_ams_corr_y, m_ams_trf_y, m_ams_time_y, = zip(med_ams_y)
        m_ums_corr_y, m_ums_trf_y, m_ums_time_y, = zip(med_ums_y)
        
        # control model
        ctrl_ass_x, ctrl_ams_x, ctrl_ums_x = [do_boosting([input_data[cond],ctrl_data_x[cond]], resp_data[cond]) 
                                                for cond in np.arange(len(conds))]
        
        c_ass_corr_x, c_ass_trf_x, c_ass_time_x, = zip(ctrl_ass_x)
        c_ams_corr_x, c_ams_trf_x, c_ams_time_x, = zip(ctrl_ams_x)
        c_ums_corr_x, c_ums_trf_x, c_ums_time_x, = zip(ctrl_ums_x)
        
        ctrl_ass_y, ctrl_ams_y, ctrl_ums_y = [do_boosting([input_data[cond],ctrl_data_y[cond]], resp_data[cond]) 
                                                for cond in np.arange(len(conds))]
        
        c_ass_corr_y, c_ass_trf_y, c_ass_time_y, = zip(ctrl_ass_y)
        c_ams_corr_y, c_ams_trf_y, c_ams_time_y, = zip(ctrl_ams_y)
        c_ums_corr_y, c_ums_trf_y, c_ums_time_y, = zip(ctrl_ums_y)
        
        
        # save it...
        outdir = './data/boosting_mediation'
        
        results = {'fit_simple': {'ass': {'r_rank': s_ass_corr[0].x,
                                        'trf': s_ass_trf[0].x,
                                        'time': np.array(s_ass_time[0])},
                                'ams': {'r_rank': s_ams_corr[0].x,
                                        'trf': s_ams_trf[0].x,
                                        'time': np.array(s_ams_time[0])},
                                'ums': {'r_rank': s_ums_corr[0].x,
                                        'trf': s_ums_trf[0].x,
                                        'time': np.array(s_ums_time[0])}},
                        'fit_mediation_x': {'ass': {'r_rank': m_ass_corr_x[0].x,
                                        'trf': m_ass_trf_x[0][0].x,
                                        'time': np.array(m_ass_time_x[0])},
                                'ams': {'r_rank': m_ams_corr_x[0].x,
                                        'trf': m_ams_trf_x[0][0].x,
                                        'time': np.array(m_ams_time_x[0])},
                                'ums': {'r_rank': m_ums_corr_x[0].x,
                                        'trf': m_ums_trf_x[0][0].x,
                                        'time': np.array(m_ums_time_x[0])}},
                        'fit_control_x': {'ass': {'r_rank': c_ass_corr_x[0].x,
                                        'trf': c_ass_trf_x[0][0].x,
                                        'time': np.array(c_ass_time_x[0])},
                                'ams': {'r_rank': c_ams_corr_x[0].x,
                                        'trf': c_ams_trf_x[0][0].x,
                                        'time': np.array(c_ams_time_x[0])},
                                'ums': {'r_rank': c_ums_corr_x[0].x,
                                        'trf': c_ums_trf_x[0][0].x,
                                        'time': np.array(c_ums_time_x[0])}},
                        'fit_mediation_y': {'ass': {'r_rank': m_ass_corr_y[0].x,
                                        'trf': m_ass_trf_y[0][0].x,
                                        'time': np.array(m_ass_time_y[0])},
                                'ams': {'r_rank': m_ams_corr_y[0].x,
                                        'trf': m_ams_trf_y[0][0].x,
                                        'time': np.array(m_ams_time_y[0])},
                                'ums': {'r_rank': m_ums_corr_y[0].x,
                                        'trf': m_ums_trf_y[0][0].x,
                                        'time': np.array(m_ums_time_y[0])}},
                        'fit_control_y': {'ass': {'r_rank': c_ass_corr_y[0].x,
                                        'trf': c_ass_trf_y[0][0].x,
                                        'time': np.array(c_ass_time_y[0])},
                                'ams': {'r_rank': c_ams_corr_y[0].x,
                                        'trf': c_ams_trf_y[0][0].x,
                                        'time': np.array(c_ams_time_y[0])},
                                'ums': {'r_rank': c_ums_corr_y[0].x,
                                        'trf': c_ums_trf_y[0][0].x,
                                        'time': np.array(c_ums_time_y[0])}}}
        
        
        savemat(join(outdir, subject + '.mat'),results)        
        
        
        
        
                
