%% clear...
clear all
close all

%% set vars...
addpath('./helpers/jschubert/myfuns/')
ll_dir = './data/mTRF/eye/lambdas/';
all_models = {'c'; 'ce'; 'co'};
all_subjects = js_getsubjectsfrom(fullfile(ll_dir, all_models{1}));

ll_range = 10.^(-5:1:5);

%% get data...
for imod = 1:length(all_models)
    ll_mod_dir = fullfile(ll_dir, all_models{imod});
    
    all_err = [];
    for isub = 1:length(all_subjects)
        load(fullfile(ll_mod_dir, [all_subjects{isub} '.mat']), 'll_all')
        all_err(:,isub) = mean(ll_all.err,2); 
    end %for isub
    
    %% avg lambdas...
    err_avg = mean(all_err,2);

    %% get opt lambda...
    [~, i_err] = min(err_avg);
    lambda_opt = ll_range(i_err);
    sprintf('optimal lambda for %s is %d', all_models{imod}, lambda_opt)

    %% save it...
    f_name = sprintf('lambda4%s.mat', all_models{imod});
    save(fullfile(ll_dir, f_name), 'lambda_opt')
    
end %for imod