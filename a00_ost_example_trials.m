%% clear...
clear all
close all

%% init obob ownft...
addpath('/mnt/obob/obob_ownft'); %change to where obob_ownft lives...
obob_init_ft; % Initialize obob_ownft

%% set vars...
addpath('./helpers/jschubert/myfuns');
data_dir = './data/aligned/eye';
all_subjects = js_getsubjectsfrom(data_dir);

out_dir = './data/example_trials';
if ~exist(out_dir, 'dir')
    mkdir(out_dir)
end %if

%% loop subs to find highest correlating trial...
max_xr = -1;
max_yr = -1;
for is = [5 27]
    max_xr = -1;
    max_yr = -1;
    %% load data...
    load(fullfile(data_dir, [all_subjects{is} '.mat']), 'data_all')

    %% select single speaker condition...
    cfg = [];
    cfg.trials = data_all.trialinfo(:,1) == 2;
    data_ss = ft_selectdata(cfg, data_all);

    %% loop trials...
    for it = 1:length(data_ss.trial)
        cur_xr = corr(data_ss.trial{it}(1,:)', data_ss.trial{it}(6,:)');
        cur_yr = corr(data_ss.trial{it}(2,:)', data_ss.trial{it}(6,:)');
        if cur_xr > max_xr
            max_xr = cur_xr;
            max_xenv = rescale(data_ss.trial{it}(6,:));
            max_xgaze = rescale(data_ss.trial{it}(1,:));
            max_xtime = data_ss.time{it};
        end %if
        if cur_yr > max_yr
            max_yr = cur_yr;
            max_yenv = rescale(data_ss.trial{it}(6,:));
            max_ygaze = rescale(data_ss.trial{it}(2,:));
            max_ytime = data_ss.time{it};
        end %if
    end %fo rit
    figure;
    plot(max_xtime, rescale(max_xenv));
    hold on;
    plot(max_xtime, rescale(max_xgaze));

    figure;
    plot(max_ytime, rescale(max_yenv));
    hold on;
    plot(max_ytime, rescale(max_ygaze));
    
    %% save for plot in python...
    save(fullfile(out_dir, [all_subjects{is} '.mat']), 'max_xenv', 'max_xgaze', 'max_xtime', ...
        'max_yenv', 'max_ygaze', 'max_ytime')
    
end %for is
