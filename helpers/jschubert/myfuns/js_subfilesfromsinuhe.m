function all_files = js_subfilesfromsinuhe(study_acronym, subject_id, maxfiltered)
  
  % js_subfilesfromsinuhe: given the study_acronym and the subject_id as input this function gives
  % you a dict with all file_names for this subject in sinuhe
  %
  % use: all_files = js_subfilesfromsinuhe(study_acronym, subject_id, maxfiltered)
  
  % per default it searches for maxfiltered files (else use false):
  if nargin < 3 || maxfiltered
    max_extension = '_trans_sss';
  else
    max_extension = '';
  end
  
  datadir = ['/mnt/sinuhe/data_raw/',study_acronym,'/subject_subject/**/',subject_id,'*',max_extension,'.fif'];

  D = dir(datadir);
  if length(D) <1
    error(['no fif-files found in',datadir,'for subject: "',subject_id,'"'])
  end
  all_files = cell(1,length(D));
  for i = 1:length(D) % loop through all fif-file names
    ifilename = D(i).name;
    all_files{i} = ifilename; % returns all filenames for that subject
  end
    
end