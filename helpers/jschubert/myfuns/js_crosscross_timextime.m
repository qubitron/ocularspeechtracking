function [acc result_acc] = js_crosscross_timextime(cfg,train_trials,train_labels,test_trials,test_labels)

% js_crosscross_timextime performs k-fold (default = 5) crossvalidtion for cross-decoding
% (trained and tested on different datasets)
%
% it takes exactly the same inpput arguments as Gps version of
% mv_classify_timextime (cfg,train_trials,train_labels,test_trials,test_labels)
%
% the output is a timextime matrics with decoding accuracies (note that
% results such as weights etc. are not yet implemented, meanwhile result_acc will be empty)
%
% this function calls a function from Gps version of MVPA-Light (make sure you have it initialized)!

%% check input data
if ~isfield(cfg,'k')
    fprintf('You did not specify "k", using the default of 5-fold crossvalidtion\n')
    cfg.k = 5;
end

if (size(train_trials,1) == size(test_trials,2)) && (all(reshape(train_trials,1,[]) == reshape(test_trials,1,[])))
    fprintf(['Train and test set appears to be the same...\n',...
        'Performing normal %d-fold cross-validation with %d repetitions.\n'],cfg.k,cfg.k)
    fprintf('Data has %d samples, %d features, %d time points, and %d classes.\n', [size(train_trials), length(unique(train_labels))])
    cfg.trainistest = 1;
else % cross-decoding
    if size(train_trials,2) ~= size(test_trials,2)
        error('Train and test set appear to have a different feature-dimension. Cannot perform cross-decoding!')
    elseif size(train_trials,3) ~= size(test_trials,3)
        error('Train and test set appear to have a different time-dimension. Cannot perform cross-decoding!')
    elseif length(unique(train_labels)) ~= length(unique(test_labels))
        error('Train and test set appear to have a different number of classes. Cannot perform cross-decoding!')
    end
    fprintf(['Train and test set are different...\n',...
        'Performing special %d-fold cross-crossvalidation with %d repetitions...\n'],cfg.k,cfg.k)
    fprintf('Train data has %d samples, %d features, %d time points, and %d classes.\n', [size(train_trials), length(unique(train_labels))])
    fprintf('Test data has %d samples, %d features, %d time points, and %d classes.\n', [size(test_trials), length(unique(test_labels))])
    cfg.trainistest = 0;
end

if length(train_labels) ~= size(train_trials,1) || length(test_labels) ~= size(test_trials,1)
    error('Number of trials and number of labels seem to be inconsistent!')
end

if length(unique(train_labels))>2 && strcmp(cfg.classifier,'lda')
    warning('foo:bar',['You selected a lda classfier but you seem to have more than 2 classes!\n',...
        'Chossing a multiclass_lda instead...'])
    cfg.classifier = 'multiclass_lda';
end

%% prepare everything
cfg.feedback = 0;

datasets = cell(1,2);
datasets{1}.trial = train_trials;
datasets{1}.trialinfo = train_labels;
datasets{2}.trial = test_trials;
datasets{2}.trialinfo = test_labels;
acc_all = zeros(cfg.k*cfg.k,size(datasets{1}.trial,3),size(datasets{1}.trial,3));
ridx = reshape(1:cfg.k*cfg.k,cfg.k,cfg.k);

%% balance trials:
% undersample (use smallest n of trials)
fprintf('Using undersampling to balance the data\n')
min_trials = min([accumarray(datasets{1}.trialinfo,1)', accumarray(datasets{1}.trialinfo,1)']);
min_trials = floor(min_trials/cfg.k)*cfg.k;

bal_idx = cell(1,2);
bal_idx{1} = bal_crosscv(datasets{1}.trialinfo, min_trials);
if cfg.trainistest % we should use the exactly the same balancing!
    bal_idx{2} = bal_idx{1};
else
    bal_idx{2} = bal_crosscv(datasets{2}.trialinfo, min_trials);
end
for set = 1:2
    datasets{set}.trial = datasets{set}.trial(bal_idx{set},:,:);
    datasets{set}.trialinfo = datasets{set}.trialinfo(bal_idx{set});
end
%% do k-repetitions:
for rr = 1:cfg.k
    fprintf('Repetition #%d. Fold ',rr)
    %% get stratified (random) fold-assignments
    cv_idx = stra_crosscv(datasets{1}.trialinfo,cfg.k);
    %     test_size = accumarray(cv_idx,1);
    %     train_size = length(cv_idx)-test_size;
    
    %% do k-folds:
    for ff = 1:cfg.k
        fprintf('%d ',ff)
        % get train & test set
        this_train_trl = datasets{1}.trial(cv_idx ~= ff,:,:);
        this_train_lbl = datasets{1}.trialinfo(cv_idx ~= ff);
        this_test_trl = datasets{2}.trial(cv_idx == ff,:,:);
        this_test_lbl = datasets{2}.trialinfo(cv_idx == ff);
        % do timextime decoding
        acc = mv_classify_timextime(cfg, this_train_trl, this_train_lbl,...
            this_test_trl, this_test_lbl);
        acc_all(ridx(ff,rr),:,:) = acc;
    end% of k-loop
    fprintf('\n')
end% of r-loop

% calculate mean accuracy over folds for every timextime-point:
acc = squeeze(mean(acc_all));
result_acc = [];
fprintf('Calculating classifier performance... finished\n')

end% of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% balance trials to a certain number of trials (min)
% is equal to undersampling
function bal_idx=bal_crosscv(labels, trial_n)
bal_idx = [];
for lbl = 1:length(unique(labels))
    lbl_idx= find(labels == lbl);
    kp_idx = randperm(nnz(lbl_idx), trial_n);
    lbl_idx = lbl_idx(kp_idx);
    bal_idx = [bal_idx; lbl_idx];
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stratified k-fold cross-validation
% stole this code from the MVPA (because this is just clever)
function cv_idx=stra_crosscv(labels,nfold)
N = size(labels,1);
cv_idx = 1 + mod((1:N)',nfold);
idrand = labels + rand(N,1);
[~,idx] = sort(idrand);
cv_idx(idx) = cv_idx;
end