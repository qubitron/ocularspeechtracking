function binned_X = js_bin_it(X,n_bins)
  
  % js_bin_it splits an input array into equal groups according to the
  % input values (like a median-split but for more groups)
  %
  % input args:
  % X = input valus (must be a vector!)
  % n_bins = number of groups to create
  %
  % output is a Nx2 matrix with input values in the first column and group
  % labels in the second column!

  if size(X,1) == 1 % turn row vector into col vector
    X = X';
  end
  idx = 1:length(X);
  withidx = [idx', X]; % add idx
  sorted = sortrows(withidx,2); % sort it

  n_per_bin = round(length(X) / n_bins);

  % add bin-labels in 3rd column:
  start = 1;
  label = 1;
  for i = 1:n_bins
    stop = start + n_per_bin-1;
    if i == n_bins
      sorted(start:end,3) = label;
    else
      sorted(start:stop,3) = label;
    end
    start = stop+1;
    label = label + 1;

  end

  binned_X = sortrows(sorted,1); % get orig order
  binned_X(:,1) = []; % and delete idx
  
  
end