function [perf, result] = mv_classify_timextime(cfg, X, clabel, X2, clabel2)
% Time x time generalisation. A classifier is trained on the training data
% X and validated on either the same dataset X. Cross-validation is
% recommended to avoid overfitting. If another dataset X2 is provided,
% the classifier is trained on X and tested on X2. No cross-validation is
% performed in this case since the datasets are assumed to be independent.
%
% Note that this function does not work with *precomputed* kernels since
% they require the kernel matrix to be evaluated for different combinations
% of time points as well.
%
% Usage:
% perf = mv_classify_timextime(cfg,X,clabel,<X2, clabel2>)
%
%Parameters:
% X              - [samples x features x time points] data matrix.
% clabel         - [samples x 1] vector of class labels
% X2, clabel2    - (optional) second dataset with associated labels. If
%                  provided, the classifier is trained on X and tested on
%                  X2 using
%
% cfg          - struct with optional parameters:
% .classifier   - name of classifier, needs to have according train_ and test_
%                 functions (default 'lda')
% .hyperparameter - struct with parameters passed on to the classifier train
%                 function (default []).
% .metric       - classifier performance metric, default 'accuracy'. See
%                 mv_classifier_performance. If set to [] or 'none', the
%                 raw classifier output (labels, dvals or probabilities
%                 depending on cfg.output_type) for each sample is returned.
%                 Use cell array to specify multiple metrics (eg
%                 {'accuracy' 'auc'}
% .time1        - indices of training time points (by default all time
%                 points in X are used)
% .time2        - indices of test time points (by default all time points
%                 in X are used)
% .feedback     - print feedback on the console (default 1)
%
% CROSS-VALIDATION parameters:
% .cv           - perform cross-validation, can be set to 'kfold',
%                 'leaveout', 'holdout', 'predefined' or 'none' (default 'kfold')
% .k            - number of folds in k-fold cross-validation (default 5)
% .p            - if cv='holdout', p is the fraction of test samples
%                 (default 0.1)
% .stratify     - if 1, the class proportions are approximately preserved
%                 in each fold (default 1)
% .repeat       - number of times the cross-validation is repeated with new
%                 randomly assigned folds (default 1)
% .fold         - if cv='predefined', fold is a vector of length
%                 #samples that specifies the fold each sample belongs to
%
% PREPROCESSING parameters: and
% .preprocess         - cell array containing the preprocessing pipeline. The
%                       pipeline is applied in chronological order
% .preprocess_param   - cell array of preprocessing parameter structs for each
%                       function. Length of preprocess_param must match length
%                       of preprocess
%
% Returns:
% perf          - time1 x time2 classification matrix of classification
%                 performances corresponding to the selected metric. If
%                 metric='none', perf is a [r x k x t] cell array of
%                 classifier outputs, where each cell corresponds to a test
%                 set, k is the number of folds, r is the number of
%                 repetitions, and t is the number of training time points.
%                 Each cell contains [n x t2] elements, where n is the
%                 number of test samples and t2 is the number of test time
%                 points. If multiple metrics are requested, perf is a cell array
% result        - struct with fields describing the classification result.
%                 Can be used as input to mv_statistics and mv_plot_result
% testlabel     - [r x k] cell array of test labels. Can be useful if
%                 metric='none'

% (c) Matthias Treder

fprintf('Using modified private function "mv_classify_timextime"\n');
if nargout ~= 2
    warning('You defined only 1 output argument - all extra calulations in this private function will be useless!')
end

X = double(X);
if ndims(X)~= 3, error('X must be 3-dimensional'), end
if nargin > 3
    X2 = double(X2);
end

mv_set_default(cfg,'classifier','lda');
mv_set_default(cfg,'hyperparameter',[]);
mv_set_default(cfg,'metric','accuracy');
mv_set_default(cfg,'time1',1:size(X,3));
mv_set_default(cfg,'feedback',1);

mv_set_default(cfg,'sample_dimension',1);
mv_set_default(cfg,'dimension_names',{'samples','features','time points'});
mv_set_default(cfg,'preprocess',{});
mv_set_default(cfg,'preprocess_param',{});

[cfg, clabel, n_classes, n_metrics] = mv_check_inputs(cfg, X, clabel);

% adapt xtra stuff for 2-class as well as multiclass application...
if n_classes > 2, cl_dim = n_classes; w = 'W'; else, cl_dim = 1; w = 'w'; end

hasX2 = (nargin==5);
if hasX2, mv_set_default(cfg,'time2',1:size(X2,3));
else,     mv_set_default(cfg,'time2',1:size(X,3));
end
nTime1 = numel(cfg.time1);
nTime2 = numel(cfg.time2);

% Number of samples in the classes
n = arrayfun( @(c) sum(clabel==c) , 1:n_classes);

% this function does not work with precomputed kernel matrices
if isfield(cfg.hyperparameter,'kernel') && strcmp(cfg.hyperparameter.kernel,'precomputed')
    error('mv_classify_timextime does not work with precomputed kernel matrices, since kernel needs to be evaluated between time points as well')
end

if nargout>1 && n_classes>2
    warning('You are using Julies private function for a multiclass problem with temporal generalization...\n%s',...
        'dvals & pred_labels calculation takes quite the computational power - matlab might crash!')
end
%% Reduce data to selected time points
X = X(:,:,cfg.time1);

if hasX2
    X2 = X2(:,:,cfg.time2);
end

%% Get train and test functions
train_fun = eval(['@train_' cfg.classifier]);
test_fun = eval(['@test_' cfg.classifier]);

%% Time x time generalisation
if ~strcmp(cfg.cv,'none') && ~hasX2
    % -------------------------------------------------------
    % One dataset X has been provided as input. X is hence used for both
    % training and testing. To avoid overfitting, cross-validation is
    % performed.
    if cfg.feedback, mv_print_classification_info(cfg,X,clabel); end
    
    % Initialise classifier outputs
    cf_output = cell(cfg.repeat, cfg.k, nTime1);
    dval_output = cell(cfg.repeat, cfg.k, nTime1);
    testlabel = cell(cfg.repeat, cfg.k);
    weights = cell(cfg.repeat, cfg.k, nTime1);
    %     bias_term = cell(cfg.repeat, cfg.k, nTime1);
    %     lambdas = cell(cfg.repeat, cfg.k, nTime1);
    
    % keep trialinfo for every rep & fold
    test_trl = cell(cfg.repeat,cfg.k);
    trl_nr = 1:size(X,1);
    
    for rr=1:cfg.repeat                 % ---- CV repetitions ----
        if cfg.feedback, fprintf('Repetition #%d. Fold ',rr), end
        
        % Define cross-validation
        CV = mv_get_crossvalidation_folds(cfg.cv, clabel, cfg.k, cfg.stratify, cfg.p, cfg.fold);
        
        for kk=1:CV.NumTestSets                      % ---- CV folds ----
            if cfg.feedback, fprintf('%d ',kk), end
            
            % Get train and test data
            [Xtrain, trainlabel, Xtest, testlabel{rr,kk}] = mv_select_train_and_test_data(X, clabel, CV.training(kk), CV.test(kk), 0);
            % get info
            test_trl{rr,kk} = trl_nr(CV.test(kk));
            
            if ~isempty(cfg.preprocess)
                % Preprocess train data
                [tmp_cfg, Xtrain, trainlabel] = mv_preprocess(cfg, Xtrain, trainlabel);
                
                % Preprocess test data
                [~, Xtest, testlabel{rr,kk}] = mv_preprocess(tmp_cfg, Xtest, testlabel{rr,kk});
            end
            
            % ---- Test data ----
            % Instead of looping through the second time dimension, we
            % reshape the data and apply the classifier to all time
            % points. We then need to apply the classifier only once
            % instead of nTime2 times.
            
            % permute and reshape into [ (trials x test times) x features]
            % samples
            Xtest= permute(Xtest, [1 3 2]);
            Xtest= reshape(Xtest, numel(testlabel{rr,kk})*nTime2, []);
            
            % ---- Training time ----
            for t1=1:nTime1
                
                % Training data for time point t1
                Xtrain_tt= squeeze(Xtrain(:,:,t1));
                
                % Train classifier
                cf= train_fun(cfg.hyperparameter, Xtrain_tt, trainlabel);
                
                % Obtain classifier output (labels, dvals or probabilities)
                cf_output{rr,kk,t1} = reshape( mv_get_classifier_output(cfg.output_type, cf, test_fun, Xtest), numel(testlabel{rr,kk}),[]);
                % also get dvals
                dval_output{rr,kk,t1} = reshape( mv_get_classifier_output('dval', cf, test_fun, Xtest), numel(testlabel{rr,kk}),cl_dim,[]);
                % also get weights, and bias terms and lamdas...
                weights{rr,kk,t1} = mv_stat_activation_pattern(cf.(w),Xtrain_tt, trainlabel);
                %                 bias_term{rr,kk,t1} = cf.b;
                %                 lambdas{rr,kk,t1} = cf.lambda;
            end
            
        end
        if cfg.feedback, fprintf('\n'), end
    end
    
    % Average classification performance across repeats and test folds
    avdim= [1,2];
    % get mean labels & dvals for every trial:
    fprintf('Calculating mean decision values & labels for every trial... ')
    dvals = reshape_and_sort(dval_output, test_trl, CV.NumObservations);
    dvals = squeeze(mean(dvals,1));
    % choose mean_pred_labels according to mean dvals..
    keep_dim = size(dvals);
    if n_classes > 2
        [~, pred_labels] = min(dvals,[],2);
        pred_labels = reshape(pred_labels,keep_dim(setdiff(1:end,2)));
    else
        pred_labels = double(dvals >= 0) + 2*double(dvals < 0);
    end
    % reshape and average weights...
    % sum-of-squared weights over subspace and average over foldsxrepetitions...
    weights = reshape(cat(3,weights{:}),size(X,2),n_classes-1,[],nTime1).^2;
    weights = squeeze(mean(sum(weights,2),3)); 
    fprintf('finished\n')
    
elseif hasX2
    % -------------------------------------------------------
    % An additional dataset X2 has been provided. The classifier is trained
    % on X and tested on X2. Cross-validation does not make sense here and
    % is not performed.
    cfg.cv = 'none';
    
    % Print info on datasets
    if cfg.feedback, mv_print_classification_info(cfg, X, clabel, X2, clabel2); end
    
    % Preprocess train data
    [tmp_cfg, X, clabel] = mv_preprocess(cfg, X, clabel);
    
    % Preprocess test data
    [~, X2, clabel2] = mv_preprocess(tmp_cfg, X2, clabel2);
    
    % Initialise classifier outputs
    cf_output = cell(1, 1, nTime1);
    dval_output = cell(1, 1, nTime1);
    weights = cell(1,nTime1);
%     bias_term = cell(1,nTime1);
%     lambdas = cell(1,nTime1);
    
    % permute and reshape into [ (trials x test times) x features]
    Xtest= permute(X2, [1 3 2]);
    Xtest= reshape(Xtest, size(X2,1)*nTime2, []);
    
    % ---- Training time ----
    for t1=1:nTime1
        
        % Training data for time point t1
        Xtrain= squeeze(X(:,:,t1));
        
        % Train classifier
        cf= train_fun(cfg.hyperparameter, Xtrain, clabel);
        
        % Obtain classifier output (labels or dvals)
        cf_output{1,1,t1} = reshape( mv_get_classifier_output(cfg.output_type, cf, test_fun, Xtest), size(X2,1),[]);
        % also get dvals
        dval_output{1,1,t1} = reshape( mv_get_classifier_output('dval', cf, test_fun, Xtest), size(X2,1), cl_dim, []);
        % also get weights, and bias terms and lamdas...
        
        weights{t1} = mv_stat_activation_pattern(cf.(w), Xtrain, clabel);
            
        
    end
    
    testlabel = clabel2;
    avdim = [];
    
    % reshape dval output (trl x ttrain x ttest)
    fprintf('Getting decision values & labels for every trial... ')
    if ndims(dval_output{1}) > 2
        dvals = reshape(cell2mat(dval_output),[],cl_dim,nTime1,nTime2);
    else
        dvals = reshape(cell2mat(dval_output),[],nTime1,nTime2);
    end
    
    pred_labels = reshape(cell2mat(cf_output),[],nTime1,nTime2);
    weights = sum(cat(3,weights{:}).^2,2);
    fprintf('finished\n')
    
end

%% Calculate performance metrics
if cfg.feedback, fprintf('Calculating performance metrics... '), end
perf = cell(n_metrics, 1);
perf_std = cell(n_metrics, 1);
perf_dimension_names = cell(n_metrics, 1);
for mm=1:n_metrics
    if strcmp(cfg.metric{mm},'none')
        perf{mm} = cf_output;
        perf_std{mm} = [];
    else
        [perf{mm}, perf_std{mm}] = mv_calculate_performance(cfg.metric{mm}, cfg.output_type, cf_output, testlabel, avdim);
        % performance dimension names
        perf_dimension_names{mm} = [{['train ' cfg.dimension_names{end}]} repmat({'metric'}, 1, ndims(perf{mm})-2) {['test ' cfg.dimension_names{end}]}];
    end
end
if cfg.feedback, fprintf('finished\n'), end

if n_metrics==1
    perf = perf{1};
    perf_std = perf_std{1};
    perf_dimension_names = perf_dimension_names{1};
    cfg.metric = cfg.metric{1};
end

if nargout > 1
result = [];
result.cfg                   = cfg;
result.perf                  = perf;
result.perf_std              = perf_std;

% xtra:
result.dvals = squeeze(permute(dvals,[1,2,4,3])); % [trl x ttrain x ttest]
result.pred_labels = permute(pred_labels,[1,3,2]); % [trl x ttrain x ttest]
result.weights = squeeze(weights); 
end

end % of main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output_shaped = reshape_and_sort(output,test_trl,n_trl)
% this functions proves that thinking in multidim-arrays is super cool :)
% as output is an ugly 3-dim cell containing 2d-mats (not quite 5d) thingy...
% but I want a 4/5-dim array with: rep x trl x (x classes) test_time x train_time
% because then we can just average over the 1st dim et voilà!
n_rep = size(output,1);
n_train_time = size(output,3);
n_test_time = size(output{1},ndims(output{1}));
if ndims(output{1}) > 2
    cl_dim = size(output{1},2);
else
    cl_dim = 1;
end

% different folds often contain different n but we can just concat them...
test_trl_mat = cell2mat(test_trl);
% use sorted index to later sort all trials
[~, all_idx] = sort(test_trl_mat(:));

% now we get our super 5-dim array:
super_mat = zeros(n_rep, n_trl, cl_dim, n_test_time, n_train_time);
for rr = 1:n_rep
    concat_folds = cell2mat(permute(output(rr,:,:),[2 1 3]));
    super_mat(rr,:,:,:,:) = reshape(concat_folds,[],cl_dim, n_test_time, n_train_time);
end

% squezzing into 4d because indexing works only over 1 dim...
super_flat = reshape(super_mat,[], cl_dim, n_test_time, n_train_time);
super_sorted = super_flat(all_idx,:,:,:);
output_shaped = reshape(super_sorted, n_rep, n_trl, cl_dim, n_test_time, n_train_time);

end