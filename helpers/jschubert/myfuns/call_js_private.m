function [out, varargout] = call_js_private(fun_name, varargin)
fun = str2func(fun_name);
[out, varargout{1:nargout-1}] = fun(varargin{:});

end