function [TGmat, varargout] = js_crop_TGmat(cfg, TGmat)

% js_crop_TGmat cuts out a segment of a temporal generalization matrix
% using a temporal window of interest given a cfg-struct.
% (crops training times, test times or both)
%
% input args:
% cfg.trainwin = [begin end]  %in secs - if NaN use original begin/end
% cfg.ttrain = times %array with training times (must be provided if trainwin)
% cfg.testwin = [begin end] %in secs - if NaN use original begin/end
% cfg.ttest = times %array with test times (must be provided if testwin)
% cfg.dimord = 'train_test' / 'test_train' %info about dimensions
% (default = 'train_test' => training = Y-Axis and testing = X-Axis)
%
% 
% for example use as:
% cfg = [];
% cfg.trainwin = [0 0.3] % 0-300 ms training win
% cfg.ttrain = times_train
% cfg.dimord = 'train_test'
% [TGmat, times_train] = crop_acc(cfg, TGmat)


% check dimord
if ~isfield(cfg,'dimord')
  cfg.dimord = 'train_test'; % per default: training x test
end
dimord = split(cfg.dimord,'_');
if ~any(contains(dimord,'train')) || ~any(contains(dimord,'test'))
  error('Unable to read info in cfg.dimord (should be "test_train" or "train_test")')
end

if isfield(cfg,'trainwin') % crop train time
  if ~isfield(cfg,'ttrain')
    error('found no time info for train times in cfg.ttrain')
  end
  start_stop = [cfg.ttrain(1), cfg.ttrain(end)];
  cfg.trainwin(isnan(cfg.trainwin)) = start_stop(isnan(cfg.trainwin));
else % take all
  cfg.ttrain = 1:size(TGmat,find(strcmp(dimord,'train')));
  cfg.trainwin = [cfg.ttrain(1), cfg.ttrain(end)];
  not_out = 'train';
end

if isfield(cfg,'testwin') % crop test time
  if ~isfield(cfg,'ttest')
    error('found no time info for test times in cfg.ttest')
  end
  start_stop = [cfg.ttest(1), cfg.ttest(end)];
  cfg.testwin(isnan(cfg.testwin)) = start_stop(isnan(cfg.testwin));
else % take all
  cfg.ttest = 1:size(TGmat,find(strcmp(dimord,'test')));
  cfg.testwin = [cfg.ttest(1), cfg.ttest(end)];
  not_out = 'test';
end

% get data according to dimord
X_toi = cfg.([dimord{1},'win']);
X_time = cfg.(['t',dimord{1}]);
Y_toi = cfg.([dimord{2},'win']);
Y_time = cfg.(['t',dimord{2}]);

%%%%%%%%%%%%%%%%%%%%%%%%%% actual croping %%%%%%%%%%%%%%%%%%%%%%%%
% very annnoying rounding problems
X_log_idx = ismember(round(X_time,2), round((X_toi(1):0.01:X_toi(2)),2));
Y_log_idx = ismember(round(Y_time,2), round((Y_toi(1):0.01:Y_toi(2)),2));
n_X = nnz(X_log_idx);
n_Y = nnz(Y_log_idx);
% 2d log mask:
mask_mat = false(length(X_time), length(Y_time));
mask_mat(X_log_idx,Y_log_idx) = true;
% as 2d indexing is needs a reshape
TGmat = reshape(TGmat(mask_mat),n_X,n_Y);

% optional output new times
varargout{strcmp(dimord,'train')} = X_time(X_log_idx);
varargout{strcmp(dimord,'test')} = Y_time(Y_log_idx);
if exist('not_out','var')
  varargout(strcmp(dimord,not_out)) = [];
end

end