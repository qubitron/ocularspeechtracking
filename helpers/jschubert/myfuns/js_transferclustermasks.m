function trendmask = js_gettrendmask(stat)

  % js_gettrendmask compares the timepoints of all clusters (with p < .09)
  % between a stat.struct (averaged over one timewindow) and a
  % stat4plot.struct (not averaged) and transfers the sign_mask, the
  % trend_mask and the trend_info to stat4plot.
  %
  % input:
  % stat4plot = struct result from ft_freqstatistics (not averaged)*
  % stat = struct result from ft_freqstatistics (with avgoverfreq)
  % cluster = 'pos' or 'neg'
  %
  % *stat4plot must contain the fields sign_mask, trend_mask and
  % trend_info.

  % loop for posclusters or negclusters:
  if strcmp(cluster,'pos')
    stat_cluster = stat.posclusters;
    stat_clusterlabelmat = stat.posclusterslabelmat;
    stat4plot_cluster = stat4plot.posclusters;
    stat4plot_clusterlabelmat = stat4plot.posclusterslabelmat;
  elseif strcmp(cluster,'neg')
    stat_cluster = stat.negclusters;
    stat_clusterlabelmat = stat.negclusterslabelmat;
    stat4plot_cluster = stat4plot.negclusters;
    stat4plot_clusterlabelmat = stat4plot.negclusterslabelmat;
  else
    error('cluster must be either "pos" or "neg"')
  end

  for i = 1:length(stat_cluster) % loop through all clusters:
    if stat_cluster(i).prob > 0.07
      return % only interested in clusters with p <= 0.07
    end
    times_stat = stat.time(squeeze(stat_clusterlabelmat == i)); % get timepoint from stat
    % get timepoints from stat4plot (loop through)
    tmp = squeeze(stat4plot_clusterlabelmat == i); 
    times_stat4plot = false(size(stat.time));
    for timeidx = 1:length(stat.time)
      if any(tmp(:,timeidx))
        times_stat4plot(timeidx) = true;
      end
    end
    times_stat4plot = stat.time(times_stat4plot);
    if all(ismember(times_stat,times_stat4plot)) % make sure that they overlap!!!
      % write into sign_mask or trend_mask:
      if stat_cluster(i).prob < 0.05 
        stat4plot.sign_mask(ismember(stat4plot_clusterlabelmat,i)) = true;
      else
        stat4plot.trend_mask(ismember(stat4plot_clusterlabelmat,i)) = true;
        stat4plot.trend_info(squeeze(ismember(stat_clusterlabelmat,i))) = true; !
      end
    else
      % look for the right clusternumber and take it
      for j = 1:length(stat4plot_cluster)
        % get timepoints from stat4plot (loop through)
        tmp = squeeze(stat4plot_clusterlabelmat == j); 
        times_stat4plot = false(size(stat.time));
        for timeidx = 1:length(stat.time)
          if any(tmp(:,timeidx))
            times_stat4plot(timeidx) = true;
          end
        end
        times_stat4plot = stat.time(times_stat4plot);
        % compare timepoints from stat & stat4plot
        if all(ismember(times_stat,times_stat4plot))
          break % stop second loop when they overlap...
        end
        if j == length(stat4plot_cluster) % reached only if no matching cluster was found
          error(['Found no matching cluster in stat4plot for ',cluster,'cluster ',num2str(i), ' in stat'])
        end
      end % end of second cluster loop
      %... and take cluster j instead of i
      % write into sign_mask or trend_mask:
      if stat_cluster(i).prob < 0.05 
        stat4plot.sign_mask(ismember(stat4plot_clusterlabelmat,j)) = true;
      else
        stat4plot.trend_mask(ismember(stat4plot_clusterlabelmat,j)) = true;
        stat4plot.trend_info(squeeze(ismember(stat_clusterlabelmat,i))) = true;
      end
      
    end % if (cluster timepoints overlap)

  end % loop through cluster
 

end % end of function
  
  