function arr = js_del_repetitions(arr)

  % js_del_repetitions takes an array of number and deletes all repetitions
  % (can be used to delete redundant trigger values (from multiple button responses))
  
  repetitions = false(1,length(arr));
  last = 0;
  for i = 1:length(arr)
    cur = arr(i);
    if cur == last
      rep = true;
    else
      rep = false;
    end
    repetitions(i) = rep;
    last = cur;
  end

  arr(repetitions) = [];
  
end