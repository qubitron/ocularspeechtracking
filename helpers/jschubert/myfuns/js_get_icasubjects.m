function ica_subjects = js_get_icasubjects(ica_dir,block)

if nargin == 1
    D = dir(fullfile(ica_dir,'ICA*.mat'));
    D2 = dir(fullfile(ica_dir,'reject_components*.mat'));
else
D = dir(fullfile(ica_dir,['ICA*',block,'*']));
D2 = dir(fullfile(ica_dir,['reject_components*',block,'*']));
end
if length(D) <1
    error(['Found no ICA-files in ',where])
end
allsubjects = cell(1,length(D));
for i = 1:length(D) % loop through all file names
    ifilename = D(i).name;
    isubject = ifilename(5:16);
    allsubjects{i} = isubject; % extract subject_id
end


if length(D2) <1
    ica_subjects = allsubjects;
    return
else
    for i = 1:length(D2) % loop through all fif-file names
        ifilename = D2(i).name;
        isubject = ifilename(19:30);
        donesubjects{i} = isubject; % extract subject_id
    end
    ica_subjects = setdiff(allsubjects,donesubjects);
end
end