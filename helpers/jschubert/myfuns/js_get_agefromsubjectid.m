function age = js_get_agefromsubjectid(subject_id)
% this function gives you back the age in years entering a complete
% subject_id or an array of ages given a cell-array of subject_ids
if iscell(subject_id)
    age = [];
    for i = 1:length(subject_id)
        subj = subject_id{i};
        age(i) = calyears(between(datetime(subj(1:8),'InputFormat','yyyyMMdd'),datetime('today'),'years'));
    end
else
    age = calyears(between(datetime(subject_id(1:8),'InputFormat','yyyyMMdd'),datetime('today'),'years'));
end
end