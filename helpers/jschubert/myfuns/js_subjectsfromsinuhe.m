function all_subjects = js_subjectsfromsinuhe(study_acronym, maxfilter)
  
  % js_subjectsfromsinuhe: given the study_acronym as input this function gives you a dict with
  % all (optional: maxfiltered) subject_ids in sinuhe
  % maxfilter = true or false (default = true)
  %
  % use: all_subjects = js_subjectsfromsinuhe(study_acronym, maxfilter)
  
  if nargin <2
    maxfilter = true;
  end

  if maxfilter
    datadir = ['/mnt/sinuhe/data_raw/',study_acronym,'/subject_subject/**/*_trans_sss.fif'];
  else
    datadir = ['/mnt/sinuhe/data_raw/',study_acronym,'/subject_subject/**/*.fif'];
  end
    
  D = dir(datadir);
  if length(D) <1
    error(['no fif-files found in',datadir])
  end
  allsubjects = cell(1,length(D));
  for i = 1:length(D) % loop through all fif-file names
    ifilename = D(i).name;
    isubject = ifilename(1:12);
    allsubjects{i} = isubject; % extract subject_id
  end
  all_subjects = unique(allsubjects); % returns unique subject_ids
  

end