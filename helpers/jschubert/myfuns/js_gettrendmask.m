function trend_mask = js_gettrendmask(stat, threshold)

  % js_gettrendmask gives you a mask of all clusters (with p < threshold)
  
  clusterdirs = {'pos', 'neg'};
  trend_mask = false(size(stat.mask));
  for i = 1:2 % pos & neg
      cur_dir = clusterdirs{i};
      if ~isfield(stat,[cur_dir,'clusters'])
          continue % with next iteration
      end
      cluster = stat.([cur_dir,'clusters']);
      labelmat = stat.([cur_dir,'clusterslabelmat']);
      
      for clusternr = 1:length(cluster) % loop through all clusters up to threshhold
          if cluster(clusternr).prob > threshold
              break % out of loop through clusters
          elseif cluster(clusternr).prob < 0.05
              continue % with next cluster
          end
          trend_mask(labelmat == clusternr) = true;
      end % of loop through clusters
      
  end % of loop through pos % neg
 
end % end of function
  
  