function fname = js_fnamefromsinuhe(study_acronym, subject_id, block, maxfiltered)

% js_fnamefromsinuhe: given the study_acronym and the subject_id and the block as input this
% function gives you the path/name for the respective fif-file
% use: fname = getfromsinuhe(study_acronym, subject_id, block)

% per default it searches for maxfiltered files (else use false):
if nargin < 4 || maxfiltered
  max_extension = '*_trans_sss';
else
  max_extension = '*';
end

% check whether there are multiple files (for example recording > 10kHz
% will be splitted into multiple files

datadir = ['/mnt/sinuhe/data_raw/',study_acronym,'/subject_subject/**/',subject_id,'_',block,max_extension,'.fif'];


D = dir(datadir);
if length(D) <1
  error(['no fif-files found in',datadir,' for subject: "',subject_id,'" and block: "',block,'"'])
elseif length(D) == 1
  fname = [D.folder,'/',D.name];
else % multiple files found
  fname = cell(1,length(D));
  for i = 1:length(D) % loop through all fif-file names
    % remove files that are read because of higher blk-nr (such as 11)
    ifilename = [D(i).folder,'/',D(i).name];
    crit_idx = strfind(ifilename,block)+length(block);
    if isstrprop(ifilename(crit_idx),'digit')
      continue
    end
    fname{i} = ifilename; % returns all filenames for that
  end
  fname(cellfun(@isempty,fname)) = [];
  if length(fname) == 1
    fname = fname{1}; % return string
  else % return cell array of split-files
    warning(['found multiple files for subject: "',subject_id,'" and block: "',block,'", fname will be a cell-array of fnames']);
    % sort it (as splitted files are named in a stupid way)
    [~, idx] = sort(cellfun(@length,fname));
    fname = fname(idx);
  end
end



end