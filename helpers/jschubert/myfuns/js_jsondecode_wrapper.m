function data_allsubjects = js_jsondecode_wrapper(txtfile)
% js_jsondecode_wrapper is a wrapper function to read json textfiles (containing data
% of multiple subjects) as a workaround to prevent JSON syntax error due to
% extra text at the end of each subject.
%
% The input is a complete filename (incl. path to file) of the textfile.
% The output is a cell array with N cells for N Subjects, each containing a
% cell array of trial-structs!
%
% use as: data_allsubjects = js_jsondecode(wrapper(txtfile);

% check whether file exists
if ~isfile(txtfile)
    error(['Unable to read file ',txtfile,'. No such file or directory.'])
end
% read in whole textfile
fid = fopen(txtfile);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);

subi = 1;
while ~isempty(str) % "read subject and delete it .. do this over and over...
    try
        data = jsondecode(str); % extra text at end of subject throws error
    catch ME
        maxlenofchar = length(num2str(length(str)));
        endofsubj = str2double(ME.message(end-(14+maxlenofchar-1):end-14))-1; % get position where subj ends
        data = jsondecode(str(1:endofsubj)); % get the data of only this one subject
    end
    
    data_allsubjects{subi} = data;
    
    % delete subject from str and continue with next subject...
    if endofsubj < length(str)
        str(1:endofsubj) = [];
    else
        str = [];
    end
    subi = subi+1;
end


end