function data = js_avgfordecoding(data, n_avg)

% js_avgfordecoding takes an epoched data struct and always averages n_avg
% trials of every condition and stores them into a new data struct
%
% make sure data.trialinfo contains only 1 column with labels (1, 2...)
% ready for decoding with MVPAlight
%
% this function calls functions from fieldtrip (make sure you have it initialized)!

if ~isfield(data,'dimord')
    cfg = [];
    cfg.keeptrials='yes';
    data = ft_timelockanalysis(cfg, data);
end

if ~strcmp(data.dimord,'rpt_chan_time')
    error('data dimensions do not correspond to "rpt_chan_time", so you probably dont want to average over first dimension!')
end

% shuffle trials before subaveraging!!!!
% for more info see for example:
% Görgen, K., Hebart, M. N., Allefeld, C., & Haynes, J. D. (2018).
% The same analysis approach: Practical protection against the pitfalls of novel neuroimaging analysis methods.
% Neuroimage, 180, 19-30.

shuffle_idx = randperm(length(data.trialinfo));
data.trialinfo = data.trialinfo(shuffle_idx,:);
data.trial = data.trial(shuffle_idx,:,:);

% split up into labels
labels = unique(data.trialinfo);

trials_all = [];
trialinfo_all = [];
for lbl = 1:length(labels)
    lbl_data = data.trial(data.trialinfo == lbl, :,:);
    
    n_pseudotrials = floor(size(lbl_data,1)/n_avg);
    % reshape data (break trial dim into 2 dims)
    lbl_data = reshape(lbl_data(1:n_avg*n_pseudotrials,:,:), n_avg, n_pseudotrials, size(lbl_data,2), size(lbl_data,3));
    % subaverage (over first dim)
    lbl_data_avg = squeeze(mean(lbl_data));
    trials_all = [trials_all; lbl_data_avg];
    trialinfo_all = [trialinfo_all; zeros(n_pseudotrials,1) + lbl];
end

data.trial = trials_all;
data.trialinfo = trialinfo_all;

end