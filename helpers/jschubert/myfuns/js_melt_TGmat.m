function TGlong = js_melt_TGmat(TGmat, times_Xaxis, times_Yaxis)

  % js_melt_TGmat takes a matrix (e.g. a temporal generalization matrix)
  % and melts it into a long format (one column)
  %
  % first input argument (TGmat) should be the data matrix
  % second input the info (times) for the X-axis
  % third input the info times) for the Y-axis
  % (if not provided per default Y-Axis = X-Axis)
  %
  % output is a Nx3 matrix: (1st col= X-Coord., 2nd col= Y-Coord, 3rd col=
  % values) - this can (for example) be used for plotting in R.
  
  xtimes = times_Xaxis;
  if nargin < 3
    if size(TGmat,1) == size(TGmat,2) % make sure that mat is sym.
      ytimes = times_Xaxis;
    else
      error('Got info of only 1 axis but TGmat seems not to be symmetrical!')
    end
  else
    ytimes = times_Yaxis;
  end
  
  datalong = TGmat(:); % columwise
  ylong = zeros(length(datalong),1);
  xlong = zeros(length(datalong),1);

  ij = 1;
  for i = 1:length(xtimes)
    x = xtimes(i);
    for j = 1:length(ytimes)
    y = ytimes(j);
    ylong(ij) = y;
    xlong(ij) = x;
    ij = ij+1;
    end
  end

  TGlong = [xlong, ylong, datalong];
  
end