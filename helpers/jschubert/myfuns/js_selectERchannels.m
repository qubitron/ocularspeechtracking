function selectedchannels = js_selectERchannels(cfg_super, data_epoched)

  % js_selectERchannels takes epoched data and selects N channels with the
  % strongest average evoked response to an event at time 0
  %
  % optional input is a cfg struct with fields:
  %
  % cfg.toilim = [tmin tmax] timewindow of interest for the evoked response
  %              default = 50 - 300ms
  %
  % cfg.timefun = @function function that is used to aggregate data over
  %               time (e.g. mean, sum, max...), default = mean
  %
  % cfg.n_chans = N number of channels to be selected, default = 62 (20%)
  %
  % this function calls functions from fieldtrip (make sure you have it initialized)!
  
  
  % check cfg_super & add defaults:
  if nargin <2
    data_epoched = cfg_super; % without cfg first arg is data
    cfg_super = [];
  end
  
  if ~ isfield(cfg_super,'toilim')
    cfg_super.toilim = [0.05 0.3];
  end
  
  if ~ isfield(cfg_super,'timefun')
    cfg_super.timefun = @mean;
  end
  
  if ~ isfield(cfg_super,'n_chans')
    cfg_super.n_chans = 62; % = 20% of 306
  end
  
  % crop time window (if too long)
  
  if data_epoched.time{1}(1) < -0.1 || data_epoched.time{1}(end) > 0.4
    tmin = max(data_epoched.time{1}(1),-0.1);
    tmax = min(data_epoched.time{1}(end),0.4);
    cfg = [];
    cfg.toilim = [tmin tmax];
    data = ft_redefinetrial(cfg, data_epoched);
  else
    tmin = data_epoched.time{1}(1);
  end
  
  % average response
  cfg=[];
  cfg.demean='yes';
  cfg.baselinewindow=[tmin 0];
  data=ft_preprocessing(cfg, data);
  
  cfg = [];
  cfg.trials           = 'all';
  avg = ft_timelockanalysis(cfg,data);
  
  % get channels with strongest response
  
  % define time window of interest:
  tmpidx = find(round(avg.time, 2) == cfg_super.toilim(1));
  toidx(1) = tmpidx(1);
  tmpidx = find(round(avg.time, 2) == cfg_super.toilim(2));
  toidx(2) = tmpidx(1);

  m_resp = cfg_super.timefun(abs((avg.avg(:,toidx(1):toidx(2)))),2); % use abs

  m_resp_sorted = sort(m_resp,'descend');
  strongest_resp = m_resp_sorted(1:cfg_super.n_chans); % select N strongest

  channel_idx = ismember(m_resp, strongest_resp); % get channels
  selectedchannels = avg.label(channel_idx); % get channels

end