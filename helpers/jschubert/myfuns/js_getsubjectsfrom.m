function allsubjects = js_getsubjectsfrom(folder)

D = dir(fullfile(folder,'*.mat'));
if length(D) <1
    D = dir(fullfile(folder,'*.csv'));
end
if length(D) <1
    warning(['Found no mat-files or csv-files in ',folder])
    allsubjects = {};
    return
end
allsubjects = cell(1,length(D));
for i = 1:length(D)
    fname = D(i).name;
    allsubjects{i} = strtok(fname, '.');
    %   for chidx = 1:length(fname)
    %     if isstrprop(fname(chidx),'digit')
    %       substr = fname(chidx:chidx+11);
    %         substr = fname(chidx:chidx+11);
    %       if js_issubjectID(substr)
    %         allsubjects{i} = substr;
    %         break
    %       end
    %     end
    %   end
end
allsubjects = unique(allsubjects);

% extract the numbers
filenum = cellfun(@(x)sscanf(x,'vp%d.mat'), allsubjects);
% sort them, and get the sorting order
[~,Sidx] = sort(filenum);
% use to this sorting order to sort the filenames
allsubjects = allsubjects(Sidx);
