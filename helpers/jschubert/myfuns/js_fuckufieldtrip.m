function data = js_fuckufieldtrip(data)
% js_fuckufieldtrip rearranges the trials {1 x n_trials cell}
% to the 3D-Matrix version {n_trails x n_chans x n_timepoints double}

% change time
data.time = data.time{1}; % must be equal for all trials!

% change trial
data.trial =  cat(3,data.trial{:});
data.trial = permute(data.trial,[3,1,2]);
data.dimord = 'rpt_chan_time';

end