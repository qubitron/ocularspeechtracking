function data_bal = js_balancetrials(data,n_trials,labels)

% js_balancetrials takes an epoched data struct and randomly selects
% n_trials of every class (given by data.trialinfo col 1 = default)!
%
% alternatively you can enter a third (optional) input argument with labels
% then these labels will be used instead of data.trialinfo col 1
%
% this function calls functions from fieldtrip (make sure you have it initialized)!

if nargin <= 2
    labels = data.trialinfo(:,1);
elseif length(labels) ~= length(data.trialinfo)
    error('Number of Labels are unequal to the number of trials found!')
end

bal_idx = [];
for lbl = 1:length(unique(labels))
    lbl_idx= find(labels == lbl);
    kp_idx = randperm(nnz(lbl_idx), n_trials);
    lbl_idx = lbl_idx(kp_idx);
    bal_idx = [bal_idx; lbl_idx];
end

cfg = [];
cfg.trials = bal_idx;
data_bal = ft_selectdata(cfg,data);

end