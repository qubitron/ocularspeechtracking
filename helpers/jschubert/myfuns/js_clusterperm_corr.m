function stat = js_clusterperm_corr(cfg, timeseries, corrvar)
% this function calculates a two-sided cluster-based-permutation-correlation test
% over a single (e.g. timeseries) dimension.
% it takes the following input arguments:
%
% cfg-struct with fields:
% cfg.nperm = % number of permutations (default = 10000)
% cfg.alpha = % sign.-threshold (default = 0.05)
% cfg.minnb = % min of adjacent points to form a cluster (default = 2)

% timeseries = e.g. brain data of shape: time x obs
% corrvar = e.g. behavioral variable of shape: 1 x obs

%% function input check:
% check nargin
if nargin ~= 3
    error('function requires 3 input-arguments: cfg, timeseries, corrvar')
end

% check data dimensions
if size(corrvar,2) ~= size(timeseries,2)
    % check if transposing the data will do...
    if size(corrvar,1) == size(timeseries,2)
        warning('"corrvar" appears to have the wrong shape and will be transposed')
        corrvar = corrvar';
    elseif size(corrvar,2) == size(timeseries,1)
        warning('"timeseries" appears to have the shape obs x time and will be transposed to time x obs')
        timeseries = timeseries';
    elseif size(corrvar,1) == size(timeseries,1)
        warning('data will be transposed into time x obs')
        corrvar = corrvar';
        timeseries = timeseries';
    else
        error('the 2 variables seem to have different numbers of observations')
    end
end

% get defaults
if ~isfield(cfg,'nperm'), cfg.nperm = 10000; end
if ~isfield(cfg,'alpha'), cfg.alpha = 0.05; end
if ~isfield(cfg,'minnb'), cfg.minnb = 2; end

% get df & add to cfg...
n_obs = size(corrvar,2);
cfg.df = n_obs-1;


%% step 1: get cluster-sum stat:
% calculate the correlation coefficient
rho = corr(timeseries', corrvar', 'type', 'Pearson');
% convert correlation coefficient to t-statistic (for MCP correction): t^2 = DF*R^2 / (1-R^2)
tstat = rho*(sqrt(n_obs-2))./sqrt((1-rho.^2));

% get cluster (t-values beyond threshold) & sum them
[true_neg_clsum, neg_clmask] = get_clsum(cfg,tstat,'neg');
[true_pos_clsum, pos_clmask] = get_clsum(cfg,tstat,'pos');

%% step 2: do permutations, randomize data, redo step 1
perm_neg_clmin = [];
perm_pos_clmax = [];
for perm = 1:cfg.nperm
    % calculate the correlation coefficient
    perm_rho = corr(timeseries', corrvar(randperm(length(corrvar)))', 'type', 'Pearson'); % shuffle corrvar over subjects:
    % convert correlation coefficient to t-statistic (for MCP correction): t^2 = DF*R^2 / (1-R^2)
    perm_tstat = perm_rho*(sqrt(n_obs-2))./sqrt((1-perm_rho.^2));
    
    % get cluster (t-values beyond threshold) & sum them
    perm_neg_clsum = get_clsum(cfg,perm_tstat,'neg');
    perm_pos_clsum = get_clsum(cfg,perm_tstat,'pos');
    
    perm_neg_clmin(perm) = min(perm_neg_clsum);
    perm_pos_clmax(perm) = max(perm_pos_clsum);
    
end

%% compare permuted with real stats & get function output:
stat = [];
stat.rho = rho;

if true_neg_clsum ~= 0
    neg_clprob = zeros(1,length(true_neg_clsum));
    for cl = 1:length(true_neg_clsum)
    % conservative p-value (as fieldtrip does it = min p-value = 1/N)
    neg_clprob(cl) = (sum(perm_neg_clmin<true_neg_clsum(cl))+1)/(cfg.nperm+1);
    end
    stat.negcluster.mask = neg_clmask;
    stat.negcluster.prob = neg_clprob;
end

if true_pos_clsum ~= 0
    pos_clprob = zeros(1,length(true_pos_clsum));
    for cl = 1:length(true_neg_clsum)
    % conservative p-value (as fieldtrip does it = min p-value = 1/N)
    pos_clprob(cl) = (sum(perm_pos_clmin>true_pos_clsum(cl))+1)/(cfg.nperm+1);
    end
    stat.poscluster.mask = pos_clmask;
    stat.poscluster.prob = pos_clprob;
end

end % of main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [clsum, clmask] = get_clsum(cfg,tstat,side)
% get cluster (t-values beyond threshold) & sum them
crit_tval = tinv(1-cfg.alpha/2,cfg.df);

if strcmp(side,'neg')
    compfun = @lt;
    crit_tval = crit_tval*-1;
else
    compfun = @gt;
end

clmask = zeros(1,length(tstat));
clsum = 0;
if any(compfun(tstat,crit_tval)) % look for cluster
    clmask = get_cl(compfun(tstat,crit_tval), cfg.minnb);
    for cl = 1:max(unique(clmask))
        clsum(cl) = sum(tstat(clmask == cl));
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function labeled_clmask = get_cl(clmask, minnb)
all_idx = find(clmask);
cl = 1;
labeled_clmask = zeros(size(clmask));
labeled_clmask(all_idx(1)) = cl;
for i = 2:length(all_idx)
    if all_idx(i) > all_idx(i-1)+1 % next cluster
        cl = cl+1;
    end
    labeled_clmask(all_idx(i)) = cl;
end
% remove all below minnb
for cl = 1:max(labeled_clmask)
    if nnz(labeled_clmask == cl) < minnb
        labeled_clmask(labeled_clmask == cl) = 0;
    end
end
end