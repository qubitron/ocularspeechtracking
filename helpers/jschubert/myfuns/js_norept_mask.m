function mask = js_norept_mask(sequ)

% js_norept_mask creates a mask to get rid off all repetitions in an input
% sequence (sequ)

  last = NaN;
  mask = true(size(sequ));
  for i = 1:length(sequ)
    cur = sequ(i);
    if cur == last
      mask(i) = false;
    end
    last = cur;
  end

end