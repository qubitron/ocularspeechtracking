function js_plot_shadedline(cfg, x, Y, varargin)
% x must be an array of 1 x samplepoints
% Y must be a matrix of N x samplpoints
% you can enter more than one Y to plot
% each input will generate one line

%% check how many lines to plot
n_lines = find(cellfun(@(x) ischar(x), varargin),1);
all_data = {Y};
for icond = 2:n_lines
    all_data{icond} = varargin{icond-1};
end
varargin = varargin(n_lines:end);

%% define optics: color, linewidth, alpha, fontsize
if any(cellfun(@(x) ischar(x) && strcmpi(x, 'color'), varargin))
    idx = find(cellfun(@(x) ischar(x) && strcmpi(x, 'color'), varargin));
    set_color = varargin{idx+1};
    varargin(idx:idx+1) = [];
else
    set_color = colormap(hot(n_lines));
end

if any(cellfun(@(x) ischar(x) && strcmpi(x, 'linestyle'), varargin))
    idx = find(cellfun(@(x) ischar(x) && strcmpi(x, 'linestyle'), varargin));
    set_style = varargin{idx+1};
    varargin(idx:idx+1) = [];
else
    set_style = repmat({'-'},n_lines,1);
end

if any(cellfun(@(x) ischar(x) && strcmpi(x, 'linewidth'), varargin))
    idx = find(cellfun(@(x) ischar(x) && strcmpi(x, 'linewidth'), varargin));
    set_width = varargin{idx+1};
    varargin(idx:idx+1) = [];
else
    set_width = 2;
end

if any(cellfun(@(x) ischar(x) && contains(x, 'alpha','Ignorecase',1), varargin))
    idx = find(cellfun(@(x) ischar(x) && contains(x, 'alpha','Ignorecase',1), varargin));
    set_alpha = varargin{idx+1};
    varargin(idx:idx+1) = [];
else
    set_alpha = 0.1;
end

% todo: work on different linestyles & colors per condition...

%% plot that shit
for icond = 1:n_lines
    
    % use SEM as shade
    shade = std(all_data{icond})./sqrt(size(all_data{icond},1));
    avg = mean(all_data{icond});
    hs = fill([x x(end:-1:1)],[avg-shade ...
        avg(end:-1:1)+shade(end:-1:1)],set_color(icond,:),'linestyle','none');
    hold on
    set(hs, 'facealpha',set_alpha);
    h(icond) = plot(x, avg, 'Color', set_color(icond,:),'linestyle', set_style{icond},'linewidth', set_width, varargin{:});
end

hold off

%% define the rest
if ~isfield(cfg,'xlabel'); cfg.xlabel = ''; end
if ~isfield(cfg,'ylabel'); cfg.ylabel = ''; end
if ~isfield(cfg,'title'); cfg.title = ''; end
if ~isfield(cfg,'legend'); cfg.legend = cell(1,n_lines); end
if ~isfield(cfg,'xlim'); cfg.xlim = [min(x), max(x)]; end
if ~isfield(cfg,'ylim'); cfg.ylim = ylim; end

if isfield(cfg,'xline'); xline(cfg.xline,'k'); end
if isfield(cfg,'yline'); yline(cfg.yline,'k--'); end
    
xlim(cfg.xlim)
ylim(cfg.ylim)
legend(h,cfg.legend,'FontSize',16)
xlabel(cfg.xlabel)
ylabel(cfg.ylabel)
title(cfg.title)
set(gca,'FontSize',14)

end


