function z_vals = js_zscore(vals, zeropoint, use_stdfromzeropoint)
% uses individual zeropoint instead of mean (e.g. chance level decoding accuracy)
% and (optionally) if you want to use the standard deviation from the individual zeropoint
% (= default) set false if you want to use the normal standard deviation of the mean)
if nargin < 3 || use_stdfromzeropoint
    sum_var = 0;
    for i = 1:numel(vals)
        var = (vals(i) - zeropoint)^2;
        %disp(var)
        sum_var = sum_var + var;
    end
    std_2use = sqrt(sum_var/(numel(vals)-1));
elseif ~use_stdfromzeropoint
    std_2use = std(vals(:)); % use standard deviation from the mean
else
    error('the third input argument must be a logical!')   
end
z_vals = zeros(size(vals));
for i = 1:numel(vals)
    z_vals(i) = (vals(i) - zeropoint)/std_2use;
end
end