function logval = js_issubjectID(string)
    % js_issubjectID takes any string as input and returns a logical value
    % indicating whether the input string corresponds to our subject_id
    % naming conventions (i.e. 8 digits then 4 letters)
    
    if length(string) ~= 12
        logval = false;
    else
        if all(isstrprop(string(1:8),'digit')) || all(isstrprop(string(9:12),'alpha'))
            logval = true;
        else
            logval = false;
        end
    end
end