function js_plot_confusion(cfg, confmat)
% get stuff from cfg or default
if ~isfield(cfg,'xlabel'); cfg.xlabel = 'Predicted'; end
if ~isfield(cfg,'ylabel'); cfg.ylabel = 'True'; end
if ~isfield(cfg,'title'); cfg.title = ''; end
if ~isfield(cfg,'colormap'); cfg.colormap = 'winter'; end
if ~isfield(cfg,'annotate'); cfg.annotate = 1; end

%
imagesc(confmat)
h.ax = gca;

if isfield(cfg,'clim')
    set(h.ax,'Clim',cfg.clim);
end
h.colorbar = colorbar('peer',h.ax(1),'location','Eastoutside');
xticks(1:length(confmat))
yticks(1:length(confmat))
xlabel(cfg.xlabel)
ylabel(cfg.ylabel)
colormap(cfg.colormap)
title(cfg.title)

if isfield(cfg,'xticklabels')
    xticklabels(cfg.xticklabels)
end
if isfield(cfg,'yticklabels')
    yticklabels(cfg.yticklabels)
end

if cfg.annotate % add values
    x = repmat(1:length(confmat),length(confmat),1); % generate x-coordinates
    y = x'; % generate y-coordinates
    t = num2cell(confmat); % extact values into cells
    t = cellfun(@num2str, t, 'UniformOutput', false); % convert to string
    text(x(:), y(:), t, 'HorizontalAlignment', 'Center')
end

end