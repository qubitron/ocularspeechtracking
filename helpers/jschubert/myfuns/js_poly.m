function data_poly = js_poly(data)

n_feat = size(data,2);

%a,b,aa,ab,bb... 
% data_poly = ones(size(data,1),1,size(data,3)); % no intercept
% data_poly = cat(2,data_poly,data(:,1,:));
data_poly = data(:,1,:);
data_poly = cat(2,data_poly,data(:,1,:).^2);
for i = 2:n_feat
    data_poly = cat(2,data_poly,data(:,i,:));
    data_poly = cat(2,data_poly,data(:,i,:)).^2;
    for j = 1:i-1
        data_poly = cat(2,data_poly,data(:,i,:).*data(:,j,:));
    end
end
if n_feat > 2
    %TODO: abc?
end


end