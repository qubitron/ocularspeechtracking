function data = js_appendtimelockdata(data1, data2)
% js_appendtimelockdata appends data after ft_timelockanalysis

% TODO: variable number of datasets

  %% check wether no of channels and timepoints are identical!
  if size(data1.trial,2) ~= size(data2.trial,2)
    error('Cannot append data due to unequal number of channels!')
  end
  
  if ~all(data1.time == data2.time)
    error('Cannot append data because timepoints are not equal!')
  end
  
  data = [];
  data.trialinfo = vertcat(data1.trialinfo, data2.trialinfo);
  data.trial = vertcat(data1.trial, data2.trial);
  data.time = data1.time;
end