function arr = js_antidiag(A)
  % js_antidiag gives the antidiagonal (lower left corner to upper
  % right corner) of a nxn-matrix
  
  s = size(A,1);
  arr = A(s:s-1:end-1);
  
end