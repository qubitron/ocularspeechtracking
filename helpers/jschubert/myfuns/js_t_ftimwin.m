function t_ftimwin = js_t_ftimwin(twin, toi, foi)

% this function calculates individual time-windows for each
% frequency-of-interest to use as input arg for ft_freqanalysis
%
% it takes the follow input arguments:
% twin = [min, max] this should be timewindow from which data can be taken
%                   (note that it might be usefull to consider responses,
%                   unwanted events etc.)
% toi = [min, max] time-window of interest
% foi = this should be an array with frequencies of interest in Hz (e.g. 4:1:30)
%
% note that the maximum number of cycles is used up to 7 cycles.

%%
win_limit = min(abs(twin - toi));
win_limit = round(win_limit*2,4); % if zerophase

lowest_freq = ceil(1/win_limit); % output
if foi (1) < lowest_freq
    error('lowest frequency of interest does not fit into time window')
end

cycles_pert = ones(1,length(foi));
for n_cycles = 3:2:7
    cutoff_freq = ceil(1/(win_limit/n_cycles));
    cycles_pert(foi >= cutoff_freq) = n_cycles;
end

%% output
t_ftimwin = cycles_pert./foi; % adapted to frequ

end