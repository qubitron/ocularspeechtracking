cmap = brewermap([],'*RdBu');
linecolors = [27 158 119;217 95 2;117 112 179;231,41,138;102,166,30]/255;
contourlevels = 40;
yticks = [1 2 6 16 40];

colormap(cmap)