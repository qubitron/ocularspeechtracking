%% RUN GET OPT LAMBDA 4 EYE (all_subjects):
% add paths & init packages:
addpath('/mnt/obob/obob_ownft/'); % adjust path to where obob_ownft lives...

cfg = [];
cfg.package.plus_slurm = true;
obob_init_ft(cfg); % Initialize obob_ownft

addpath('./cluster_jobs');

% create struct for cluster job
cfg = [];
cfg.mem = '1G'; %per subj
cfg.request_time = 300;
% cfg.exclude_nodes = 'scs1-6';
slurm_struct = obob_slurm_create(cfg);

% get subjects...
addpath('./helpers/jschubert/myfuns')
data_dir = './data/aligned/eye/';
all_subjects = js_getsubjectsfrom(data_dir);
all_models = {'c'; 'ce'; 'co'}; %c=control, ce=combined_envelope, co=combined_onsets

% configure...
cfg_job = [];
cfg_job.fs = 50;
cfg_job.dir = 1;
cfg_job.tmin = -100;
cfg_job.tmax = 550;
cfg_job.lambdas = 10.^(-5:1:5);
cfg_job.method = 'ridge';
cfg_job.zeropad = 1;
cfg_job.verbose = 0;
cfg_job.corr = 'Spearman'; 
cfg_job.channel = {'x'; 'y'}; % x=horizontal, y=vertical
cfg_job.models.c = {'vis_ons'; 'audio_onset'};
cfg_job.models.ce = vertcat(cfg_job.models.c, {'envelope'});
cfg_job.models.co = vertcat(cfg_job.models.c, {'acoustic_onsets'});

% set path 2 save data:
out_dir = './data/mTRF/eye/lambdas/';

%%%% send to cluster...
slurm_struct = obob_slurm_addjob_cell(slurm_struct, 'C_GetOptLambda4Eye', ...
    all_subjects, all_models, data_dir, out_dir, cfg_job);
obob_slurm_submit(slurm_struct);