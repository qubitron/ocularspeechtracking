classdef C_FwdModelMegEye < obob_slurm_job
    %C_FWDMODELMEGEYE Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function run(obj, subject_id, model_id, ll_opt_dir, data_dir, out_dir, cfg_job)
            %C_COCKTAILEFFORTPARTIALMTRF Construct an instance of this class
            %   Detailed explanation goes here
            %% ADD PATHS FOR FIELTRIPTOOLBOX, mTRF & ownfuns:
            addpath('/mnt/obob/obob_ownft') % adjust path to where obob_ownft lives...
            obob_init_ft; % Initialize obob_ownft
            
            % add mTRF...
            addpath('/mnt/obob/staff/qgehmacher/git/mTRF-Toolbox/mtrf'); % adjust to where mTRF lives...
            
            addpath('./helpers/jschubert/myfuns');
            addpath('./cluster_jobs');
            
            %% get data...
            load(fullfile(data_dir, [subject_id '.mat']), 'data_all')
            
            %% get opt lambda...
            load(fullfile(ll_opt_dir, sprintf('lambda4%s.mat', model_id)), 'lambda_opt');
            
            %% do fwd model except single distractor...
            data_sub = [];
            for icond = 2:4
                
                %% get condition...
                cfg = [];
                cfg.trials = find(data_all.trialinfo(:,1) == icond);
                
                data_cond = ft_selectdata(cfg, data_all);
                
                %% get stim & response data...
                cfg = [];
                cfg.channel = cfg_job.channel;
                resp_data = ft_selectdata(cfg, data_cond);
                
                cfg.channel = cfg_job.models.(model_id);
                stim_data = ft_selectdata(cfg, data_cond);
                
                %% initialize ft-struct...
                ft_stats = [];
                ft_stats.label = resp_data.label;
                ft_stats.dimord = 'chan_freq';
                ft_stats.elec = data_cond.elec;
                ft_stats.freq = 1;
                
                %% transpose data for mTRF...
                resp_data = cellfun(@transpose, resp_data.trial, 'Un', 0);
                stim_data = cellfun(@transpose, stim_data.trial, 'Un', 0);
                
                %% normalize data for mTRF...
                for c = 1:length(stim_data)
                    stim_data{c} = normalize(stim_data{c},1,'norm',1);
                    resp_data{c} = normalize(resp_data{c},1,'norm',1);
                end %for
                
                %% do fwd-model on folds...
                trl_model = [];
                trf_all = {};
                resp_all = {};
                trl_r = [];
                trl_err = [];
                for trl = 1:length(resp_data)
                    %% get training and test data...
                    stim_train = stim_data(1:end ~= trl);
                    resp_train = resp_data(1:end ~= trl);

                    stim_test = stim_data(trl);
                    resp_test = resp_data(trl);
                    
                    %% get trf...
                    model = mTRFtrain(stim_train, resp_train, cfg_job.fs, cfg_job.dir, cfg_job.tmin, cfg_job.tmax, lambda_opt, 'method', cfg_job.method, 'zeropad',  cfg_job.zeropad, 'verbose', cfg_job.verbose);
                    
                    %% predict...
                    [trf, stats] = mTRFpredict(stim_test, resp_test, model, 'zeropad', cfg_job.zeropad, 'corr', cfg_job.corr, 'error', 'mae', 'verbose', cfg_job.verbose);
                    
                    %% get results...
                    trl_model(trl,:,:,:) = model.w;
                    trf_all{trl} = trf';
                    resp_all{trl} = resp_test{1}';
                    trl_r(trl,:) = stats.r;
                    trl_err(trl,:) = stats.err;
                    
                end %for trl
                model.w = squeeze(mean(trl_model));
                avg_r = mean(trl_r);
                avg_err = mean(trl_err);
                
                %% store...
                ft_stats.powspctrm = avg_r';
                ft_weights = [];
                
                for ift = 1:length(cfg_job.models.(model_id))
                   
                    ft_weights.(cfg_job.models.(model_id){ift}).label = ft_stats.label;
                    ft_weights.(cfg_job.models.(model_id){ift}).dimord = 'chan_time';
                    ft_weights.(cfg_job.models.(model_id){ift}).elec = data_cond.elec;
                    if length(cfg_job.models.(model_id)) == 1
                        ft_weights.(cfg_job.models.(model_id){ift}).avg = model.w';
                    else
                        ft_weights.(cfg_job.models.(model_id){ift}).avg = squeeze(model.w(ift,:,:))';
                    end %if
                    ft_weights.(cfg_job.models.(model_id){ift}).time = model.t;
                
                end %for
                
                %% store...
                data_sub.(sprintf('cond%d', icond)).ft_stats = ft_stats;
                data_sub.(sprintf('cond%d', icond)).ft_weights = ft_weights;
                data_sub.(sprintf('cond%d', icond)).trials_model = trl_model;
                data_sub.(sprintf('cond%d', icond)).model = model;
                data_sub.(sprintf('cond%d', icond)).trf = trf_all;
                data_sub.(sprintf('cond%d', icond)).resp = resp_all;
                data_sub.(sprintf('cond%d', icond)).trl_stats.r = trl_r';
                data_sub.(sprintf('cond%d', icond)).trl_stats.err = trl_err';
                data_sub.(sprintf('cond%d', icond)).stats.r = avg_r; 
                data_sub.(sprintf('cond%d', icond)).stats.err = avg_err;
                
            end %for icond
            
            %% save data:
            %------------------------------------------------------------------------
            fname_2save = obj.get_fname_2save(out_dir, model_id, subject_id); % saved fileName in outdir
            %
            save(fname_2save, 'data_sub');
            
        end %function
        
        % shall-run fun
        function run = shall_run(obj, subject_id, model_id, ll_opt_dir, data_dir, out_dir, cfg_job)
            run = ~exist(obj.get_fname_2save(out_dir, model_id, subject_id), 'file');
        end % of shall_run fun
        
        % get fname_2save fun
        function fname_2save = get_fname_2save(obj, out_dir, model_id, subject_id)
            save_dir = fullfile(out_dir, model_id);
            if ~exist(save_dir, 'dir')
                mkdir(save_dir)
            end %if
            fname_2save = fullfile(save_dir, [subject_id '.mat']);
        end % of get_fname_2save fun
    end %methods
end %classdef

