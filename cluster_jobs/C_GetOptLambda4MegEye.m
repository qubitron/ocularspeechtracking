classdef C_GetOptLambda4MegEye < obob_slurm_job
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function run(obj, subject_id, model_id, data_dir, out_dir, cfg_job)
            %C_COCKTAILEFFORTPARTIALMTRF Construct an instance of this class
            %   Detailed explanation goes here
            %% ADD PATHS FOR FIELTRIPTOOLBOX, mTRF & ownfuns:
            addpath('/mnt/obob/obob_ownft') % adjust path to where obob_ownft lives...
            obob_init_ft; % Initialize obob_ownft
            
            % add mTRF...
            addpath('/mnt/obob/staff/qgehmacher/git/mTRF-Toolbox/mtrf'); % adjust to where mTRF lives...
            
            addpath('./helpers/jschubert/myfuns');
            addpath('./cluster_jobs');
            
            %% get data...
            load(fullfile(data_dir, [subject_id '.mat']), 'data_all')
            
            %% do fwd model except single distractor......
            ll_all = [];
            for icond = 2:length(unique(data_all.trialinfo(:,1)))
                
                %% get condition...
                cfg = [];
                cfg.trials = find(data_all.trialinfo(:,1) == icond);
                
                data_cond = ft_selectdata(cfg, data_all);
                
                %% get stim & response data...
                cfg = [];
                cfg.channel = cfg_job.channel;
                resp_data = ft_selectdata(cfg, data_cond);
                
                cfg.channel = cfg_job.models.(model_id);
                stim_data = ft_selectdata(cfg, data_cond);
                
                %% transpose data for mTRF...
                resp_data = cellfun(@transpose, resp_data.trial, 'Un', 0);
                stim_data = cellfun(@transpose, stim_data.trial, 'Un', 0);
                
                %% normalize data for mTRF...
                for c = 1:length(stim_data)
                    stim_data{c} = normalize(stim_data{c},1,'norm',1);
                    resp_data{c} = normalize(resp_data{c},1,'norm',1);
                end %for
              
                %% iterate over predifened lambda range...
                ll_res = zeros(length(cfg_job.lambdas),2);
                for ll = 1:length(cfg_job.lambdas)
                    
                    %% cross-validation...
                    cv = mTRFcrossval(stim_data, resp_data, cfg_job.fs, cfg_job.dir, cfg_job.tmin, cfg_job.tmax, cfg_job.lambdas(ll), 'method', cfg_job.method, ... 
                            'zeropad', cfg_job.zeropad, 'error', 'mae', 'verbose', cfg_job.verbose);
                        
                    ll_res(ll,:) = [squeeze(mean(cv.r, [1 3])) squeeze(mean(cv.err, [1 3]))];
                    
                end %for ll
                ll_all.r(:,icond-1) = ll_res(:,1);
                ll_all.err(:,icond-1) = ll_res(:,2);
                
            end %for icond
            
            %% save data:
            %------------------------------------------------------------------------
            fname_2save = obj.get_fname_2save(out_dir, model_id, subject_id); % saved fileName in outdir
            %
            save(fname_2save, 'll_all');
            
        end %function
        
        % shall-run fun
        function run = shall_run(obj, subject_id, model_id, data_dir, out_dir, cfg_job)
            run = ~exist(obj.get_fname_2save(out_dir, model_id, subject_id), 'file');
        end % of shall_run fun
        
        % get fname_2save fun
        function fname_2save = get_fname_2save(obj, out_dir, model_id, subject_id)
            save_dir = fullfile(out_dir, model_id);
            if ~exist(save_dir, 'dir')
                mkdir(save_dir)
            end %if
            fname_2save = fullfile(save_dir, [subject_id '.mat']);
        end % of get_fname_2save fun
    end %methods
end %classdef
