#%% imports...
import arviz as az
import bambi as bmb
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import eelbrain as eb
import matplotlib.patheffects as pe
import seaborn as sns
from pathlib import Path
from pymatreader import read_mat
from os import listdir, makedirs
from os.path import join, isdir
from scipy.stats import zscore

import matplotlib as mpl
new_rc_params = {'text.usetex': False,
"svg.fonttype": 'none'
}
mpl.rcParams.update(new_rc_params)

#%% get encoding results...
enc_dir = Path(Path.cwd()).expanduser() / 'data' / 'mTRF' / 'eye' / 'fwd'
bhv_dir = Path(Path.cwd()).expanduser() / 'data' / 'behavioral'

all_files = sorted(listdir(join(enc_dir, 'c/')))
all_subjects = np.unique([f[:12] for f in all_files]).tolist()

# channel into separate cols (as dv)
def load_enc(fname,ch_idx,mname):
    enc_data_test = read_mat(join(enc_dir, mname+fname)) 
    enc_data_c = read_mat(join(enc_dir, 'c/'+fname))

    # get condition results...
    tmp = []
    for k in enc_data_test['data_sub'].keys():
        acc_test = enc_data_test['data_sub'][k]['stats']['r'][ch_idx]
        acc_cont = enc_data_c['data_sub'][k]['stats']['r'][ch_idx]
        acc_diff = acc_test - acc_cont
        tmp.append(acc_diff) #cond1-4
    return tmp

def load_fishz(fname,ch_idx,mname):
    enc_data_test = read_mat(join(enc_dir, mname+fname)) 
    enc_data_c = read_mat(join(enc_dir, 'c/'+fname))

    # get condition results...
    tmp = []
    for k in enc_data_test['data_sub'].keys():
        acc_test = enc_data_test['data_sub'][k]['stats']['fishz'][ch_idx]
        acc_cont = enc_data_c['data_sub'][k]['stats']['fishz'][ch_idx]
        acc_diff = acc_test - acc_cont
        tmp.append(acc_diff) #cond1-4
    return tmp

def load_bhv(fname, varname, *args):
    bhv_data = read_mat(join(bhv_dir, fname))['data_behav']
    tmp = []
    for k in bhv_data.keys():
        tmp.append(bhv_data[k][varname]) #cond1-3
    tmp.append(bhv_data['multispeaker'][varname]) # cond3 = cond4
    return tmp

def load_and_append(func, all_files, what, mname):
    res = [func(fname,what,mname) for fname in all_files]
    return np.concatenate(res,axis=0) #subj1-30

#%% get data...
enc_horizontal_ce = load_and_append(load_enc, all_files, 0, 'ce/') #first channel
enc_vertical_ce = load_and_append(load_enc, all_files, 1, 'ce/') #second channel
enc_avg_ce = (enc_horizontal_ce + enc_vertical_ce)/2 # average over both channels

enc_horizontal_co = load_and_append(load_enc, all_files, 0, 'co/')
enc_vertical_co = load_and_append(load_enc, all_files, 1, 'co/') 
enc_avg_co = (enc_horizontal_co + enc_vertical_co)/2 

# fisher-z data...
fishz_horizontal_ce = load_and_append(load_fishz, all_files, 0, 'ce/') #first channel
fishz_vertical_ce = load_and_append(load_fishz, all_files, 1, 'ce/') #second channel
fishz_avg_ce = (fishz_horizontal_ce + fishz_vertical_ce)/2 # average over both channels

fishz_horizontal_co = load_and_append(load_fishz, all_files, 0, 'co/')
fishz_vertical_co = load_and_append(load_fishz, all_files, 1, 'co/') 
fishz_avg_co = (fishz_horizontal_co + fishz_vertical_co)/2 

#behavioral data...
intelligibility = load_and_append(load_bhv, all_files, 'intelligibility', None)
engagement = load_and_append(load_bhv, all_files, 'motivation', None)
effort = load_and_append(load_bhv, all_files, 'effort', None)  

#%% create df
all_conds = ['cond1', 'cond2', 'cond3', 'cond4']#['ign_ss', 'att_ss', 'att_ms', 'ign_ms']
conditions = eb.combine([eb.Factor(all_conds)]*len(all_subjects))
subject_ids = eb.combine([eb.Factor(all_subjects, random=True, repeat=len(all_conds))])

df_ce = pd.DataFrame({'subject_id': subject_ids,
                   'condition': conditions,
                   'enc_horizontal': enc_horizontal_ce,
                   'enc_vertical' : enc_vertical_ce,
                   'enc_avg' : enc_avg_ce,
                   'fishz_horizontal': fishz_horizontal_ce,
                   'fishz_vertical' : fishz_vertical_ce,
                   'fishz_avg': fishz_avg_ce,
                   'intelligibility': intelligibility,
                   'engagement': engagement,
                   'effort': effort})

df_co = pd.DataFrame({'subject_id': subject_ids,
                   'condition': conditions,
                   'enc_horizontal': enc_horizontal_co,
                   'enc_vertical' : enc_vertical_co,
                   'enc_avg' : enc_avg_co,
                   'fishz_horizontal': fishz_horizontal_co,
                   'fishz_vertical' : fishz_vertical_co,
                   'fishz_avg': fishz_avg_co,
                   'intelligibility': intelligibility,
                   'engagement': engagement,
                   'effort': effort})

#%% rescale behavioral data to set a meaningful 0-point
# (only relevant if used as predictors)
df_ce['enc_avg_centered'] = df_ce.groupby(['condition']).intelligibility.transform(lambda x: x - np.mean(x))
df_ce['fishz_avg_centered'] = df_ce.groupby(['condition']).fishz_avg.transform(lambda x: x - np.mean(x))
df_ce['intelligibility_logit'] = df_ce.groupby(['condition']).intelligibility.transform(lambda x: np.log(x / (1-x)))

df_co['enc_avg_centered'] = df_co.groupby(['condition']).intelligibility.transform(lambda x: x - np.mean(x))
df_co['fishz_avg_centered'] = df_co.groupby(['condition']).fishz_avg.transform(lambda x: x - np.mean(x))
df_co['intelligibility_logit'] = df_co.groupby(['condition']).intelligibility.transform(lambda x: np.log(x / (1-x)))

#%% DO STATS
def get_sumstats(formula, df):
    model = bmb.Model(formula, data=df, categorical=['condition', 'subject_id'], dropna=True, family='t')
    fit_res = model.fit(tune=4000, draws=2000, chains=4, target_accept=0.95, random_seed=1770)
    model.predict(fit_res)
    return az.summary(fit_res, round_to=5), fit_res

#%% basic model (comparing all conditions against 0)
basic_avg_ce, basic_avg_ce_post = get_sumstats('enc_avg ~ 0 + condition + (1|subject_id)', df_ce)
basic_avg_co, basic_avg_co_post = get_sumstats('enc_avg ~ 0 + condition + (1|subject_id)', df_co)

# appendix...
basic_horz_ce, basic_horz_ce_post = get_sumstats('enc_horizontal ~ 0 + condition + (1|subject_id)', df_ce)
basic_horz_co, basic_horz_co_post = get_sumstats('enc_horizontal ~ 0 + condition + (1|subject_id)', df_co)
basic_vert_ce, basic_vert_ce_post = get_sumstats('enc_vertical ~ 0 + condition + (1|subject_id)', df_ce)
basic_vert_co,  basic_vert_co_post = get_sumstats('enc_vertical ~ 0 + condition + (1|subject_id)', df_co)

#%% do stats with fish z...
fishz_avg_ce, fishz_avg_ce_post = get_sumstats('fishz_avg ~ 0 + condition + (1|subject_id)', df_ce)
print(fishz_avg_ce)
fishz_avg_co, fishz_avg_co_post = get_sumstats('fishz_avg ~ 0 + condition + (1|subject_id)', df_co)
print(fishz_avg_co)

fishz_horz_ce, fishz_horz_ce_post = get_sumstats('fishz_horizontal ~ 0 + condition + (1|subject_id)', df_ce)
fishz_horz_co, fishz_horz_co_post = get_sumstats('fishz_horizontal ~ 0 + condition + (1|subject_id)', df_co)
fishz_vert_ce, fishz_vert_ce_post = get_sumstats('fishz_vertical ~ 0 + condition + (1|subject_id)', df_ce)
fishz_vert_co, fishz_vert_co_post = get_sumstats('fishz_vertical ~ 0 + condition + (1|subject_id)', df_co)

#%% make df...
basic_posteriors_ce = basic_avg_ce_post.posterior['condition'].to_numpy()
df_basic_avg_ce_post = pd.DataFrame({'single speaker\ndistractor': basic_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': basic_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': basic_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': basic_posteriors_ce[:,:,3].flatten()})
df_basic_avg_ce_post_tidy = df_basic_avg_ce_post.melt()

basic_posteriors_co = basic_avg_co_post.posterior['condition'].to_numpy()
df_basic_avg_co_post = pd.DataFrame({'single speaker\ndistractor': basic_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': basic_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': basic_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': basic_posteriors_co[:,:,3].flatten()})
df_basic_avg_co_post_tidy = df_basic_avg_co_post.melt()

horz_posteriors_ce = basic_horz_ce_post.posterior['condition'].to_numpy()
df_horz_ce_post = pd.DataFrame({'single speaker\ndistractor': horz_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': horz_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': horz_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': horz_posteriors_ce[:,:,3].flatten()})
df_horz_ce_post_tidy = df_horz_ce_post.melt()

horz_posteriors_co = basic_horz_co_post.posterior['condition'].to_numpy()
df_horz_co_post = pd.DataFrame({'single speaker\ndistractor': horz_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': horz_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': horz_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': horz_posteriors_co[:,:,3].flatten()})
df_horz_co_post_tidy = df_horz_co_post.melt()

vert_posteriors_ce = basic_vert_ce_post.posterior['condition'].to_numpy()
df_vert_ce_post = pd.DataFrame({'single speaker\ndistractor': vert_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': vert_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': vert_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': vert_posteriors_ce[:,:,3].flatten()})
df_vert_ce_post_tidy = df_vert_ce_post.melt()

vert_posteriors_co = basic_vert_co_post.posterior['condition'].to_numpy()
df_vert_co_post = pd.DataFrame({'single speaker\ndistractor': vert_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': vert_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': vert_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': vert_posteriors_co[:,:,3].flatten()})
df_vert_co_post_tidy = df_vert_co_post.melt()

#%% fihser z...
fishz_posteriors_ce = fishz_avg_ce_post.posterior['condition'].to_numpy()
df_fishz_avg_ce_post = pd.DataFrame({'single speaker\ndistractor': fishz_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_posteriors_ce[:,:,3].flatten()})
df_fishz_avg_ce_post_tidy = df_fishz_avg_ce_post.melt()

fishz_posteriors_co = fishz_avg_co_post.posterior['condition'].to_numpy()
df_fishz_avg_co_post = pd.DataFrame({'single speaker\ndistractor': fishz_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_posteriors_co[:,:,3].flatten()})
df_fishz_avg_co_post_tidy = df_fishz_avg_co_post.melt()

fishz_horz_posteriors_ce = fishz_horz_ce_post.posterior['condition'].to_numpy()
df_fishz_horz_ce_post = pd.DataFrame({'single speaker\ndistractor': fishz_horz_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_horz_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_horz_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_horz_posteriors_ce[:,:,3].flatten()})
df_fishz_horz_ce_post_tidy = df_fishz_horz_ce_post.melt()

fishz_horz_posteriors_co = fishz_horz_co_post.posterior['condition'].to_numpy()
df_fishz_horz_co_post = pd.DataFrame({'single speaker\ndistractor': fishz_horz_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_horz_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_horz_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_horz_posteriors_co[:,:,3].flatten()})
df_fishz_horz_co_post_tidy = df_fishz_horz_co_post.melt()

fishz_vert_posteriors_ce = fishz_vert_ce_post.posterior['condition'].to_numpy()
df_fishz_vert_ce_post = pd.DataFrame({'single speaker\ndistractor': fishz_vert_posteriors_ce[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_vert_posteriors_ce[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_vert_posteriors_ce[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_vert_posteriors_ce[:,:,3].flatten()})
df_fishz_vert_ce_post_tidy = df_fishz_vert_ce_post.melt()

fishz_vert_posteriors_co = fishz_vert_co_post.posterior['condition'].to_numpy()
df_fishz_vert_co_post = pd.DataFrame({'single speaker\ndistractor': fishz_vert_posteriors_co[:,:,0].flatten(),
                                    'single speaker\ntarget': fishz_vert_posteriors_co[:,:,1].flatten(),
                                    'multi speaker\ntarget': fishz_vert_posteriors_co[:,:,2].flatten(),
                                    'multi speaker\ndistractor': fishz_vert_posteriors_co[:,:,3].flatten()})
df_fishz_vert_co_post_tidy = df_fishz_vert_co_post.melt()

#%% post-hoc comparisions between target & dist
posthoc_avg_ce = get_sumstats('enc_avg ~ condition + (1|subject_id)',
                            df_ce.query('condition == "cond3" | condition == "cond4"'))

posthoc_avg_co = get_sumstats('enc_avg ~ condition + (1|subject_id)',
                            df_co.query('condition == "cond3" | condition == "cond4"')) 

posthoc_fishz_avg_ce = get_sumstats('fishz_avg ~ condition + (1|subject_id)',
                            df_ce.query('condition == "cond3" | condition == "cond4"'))
print(posthoc_fishz_avg_ce)

posthoc_fishz_avg_co = get_sumstats('fishz_avg ~ condition + (1|subject_id)',
                            df_co.query('condition == "cond3" | condition == "cond4"')) 
print(posthoc_fishz_avg_co)

#%% intelligibility...
int_fishz_avg_ce, int_fishz_avg_ce_post = get_sumstats('intelligibility_logit ~ condition * fishz_avg_centered + (1|subject_id)',
                                           df_ce.query('condition == "cond2" | condition == "cond3"'))
print(int_fishz_avg_ce)

# 1 divergence...recheck with increased target accept
model = bmb.Model('intelligibility_logit ~ condition * fishz_avg_centered + (1|subject_id)', data=df_ce.query('condition == "cond2" | condition == "cond3"'), categorical=['condition', 'subject_id'], dropna=True, family='t')
int_fishz_avg_ce_post = model.fit(tune=4000, draws=2000, chains=4, target_accept=0.99, random_seed=1770)
model.predict(int_fishz_avg_ce_post)
int_fishz_avg_ce = az.summary(int_fishz_avg_ce_post, round_to=5)
# same results

int_fishz_avg_co, int_fishz_avg_co_post = get_sumstats('intelligibility_logit ~ condition * fishz_avg_centered + (1|subject_id)',
                                           df_co.query('condition == "cond2" | condition == "cond3"'))
print(int_fishz_avg_co)

#%% subjective effort...
eft_fishz_avg_ce, eft_fishz_avg_ce_post = get_sumstats('effort ~ condition * fishz_avg_centered + (1|subject_id)',
                                           df_ce.query('condition == "cond2" | condition == "cond3"'))
print(eft_fishz_avg_ce)

eft_fishz_avg_co, eft_fishz_avg_co_post = get_sumstats('effort ~ condition * fishz_avg_centered + (1|subject_id)',
                                           df_co.query('condition == "cond2" | condition == "cond3"'))
print(eft_fishz_avg_co)

#%% make df for plots...
df_fishz_int = pd.DataFrame({'envelope tracking': int_fishz_avg_ce_post.posterior['fishz_avg_centered'].to_numpy().flatten(),
                                    'condition*envelope tracking': int_fishz_avg_ce_post.posterior['condition:fishz_avg_centered'].to_numpy().flatten()})
df_fishz_int_tidy = df_fishz_int.melt()  

df_fishz_int_co = pd.DataFrame({'onset tracking': int_fishz_avg_co_post.posterior['fishz_avg_centered'].to_numpy().flatten(),
                                    'condition*onset tracking': int_fishz_avg_co_post.posterior['condition:fishz_avg_centered'].to_numpy().flatten()})
df_fishz_int_tidy_co = df_fishz_int_co.melt()
 
df_fishz_eft_ce = pd.DataFrame({'evelope tracking': eft_fishz_avg_ce_post.posterior['fishz_avg_centered'].to_numpy().flatten(),
                                    'condition*envelope tracking': eft_fishz_avg_ce_post.posterior['condition:fishz_avg_centered'].to_numpy().flatten()})
df_fishz_eft_ce_tidy = df_fishz_eft_ce.melt()

df_fishz_eft_co = pd.DataFrame({'evelope tracking': eft_fishz_avg_co_post.posterior['fishz_avg_centered'].to_numpy().flatten(),
                                    'condition*envelope tracking': eft_fishz_avg_co_post.posterior['condition:fishz_avg_centered'].to_numpy().flatten()})
df_fishz_eft_co_tidy = df_fishz_eft_co.melt()   

#%% we plot regression lines for intelligibilty right here...
def plot_my_mixed_model_cat(df, fitted, pal, conds):

    fig, ax = plt.subplots(figsize=(5, 5))

    for idx, cond in enumerate(conds):

        #intercept = fitted.posterior['condition'][:,:,idx].values

        if cond == conds[0]:
            slope= fitted.posterior["fishz_avg_centered"].values
            intercept = fitted.posterior['Intercept'].values

        else:
            slope = (fitted.posterior["fishz_avg_centered"].values + fitted.posterior["condition:fishz_avg_centered"].values[:,:,idx-1])
            intercept =  fitted.posterior['condition'][:,:,0].values + fitted.posterior['Intercept'].values
            ref_intercept = fitted.posterior['Intercept'].values

        #range of values 
        cur_df = df.query(f'condition == "{conds[idx]}"').reset_index()
        x_range = np.array([cur_df['fishz_avg_centered'].min(),
                            cur_df['fishz_avg_centered'].max(),])
        
        regression_line = intercept.mean() + slope.mean() * x_range
        
        regression_area = intercept.reshape(2, -1) + slope.reshape(2, -1) * x_range[:, None]
        
        sns.scatterplot(x='fishz_avg_centered', y='intelligibility_logit', data=cur_df, color=pal[idx], s=40, alpha=0.8, ax=ax)
        ax.plot(x_range, regression_area, color=pal[idx], zorder=1, lw=0.1, alpha=0.05)
        ax.plot(x_range, regression_line, color=pal[idx], linewidth=3, path_effects=[pe.Stroke(linewidth=6, foreground='w'), pe.Normal()],)

        ax.set_xlabel('fishz_avg_centered')
        ax.set_title(f'condition*ocular speech tracking')

    sns.despine()

    return fig

fig_dir = Path(Path.cwd()).expanduser() / 'data' / 'figures'
if not isdir(fig_dir):
    makedirs(fig_dir)
    
sns.set_style("ticks", rc={'xtick.direction': 'in', 'ytick.direction': 'in'})
sns.set_context("paper")#, rc={'font-family': 'Arial', "font.size":12, "axes.titlesize":14,"axes.labelsize":11})
cmap_diff = sns.color_palette("rocket",4)

#% envelope...
f = plot_my_mixed_model_cat(df = df_ce.query('condition == "cond2" | condition == "cond3"'),
                        fitted=int_fishz_avg_ce_post, pal=cmap_diff[1:3], conds=['cond2','cond3'])
plt.xlim(-0.08, 0.1)
plt.ylim(0, 7.5)
plt.xticks([-0.08, -0.04, 0, 0.04, 0.08], ['-0.08', '-0.04', '0', '0.04', '0.08'])
plt.yticks([2, 4, 6], ['2', '4', '6'])
plt.savefig(join(fig_dir, 'fishz_condition_intelligibility_ce.svg'), format='svg', dpi=1200)
plt.show()

#%% acoustic onsets...
f = plot_my_mixed_model_cat(df = df_co.query('condition == "cond2" | condition == "cond3"'),
                        fitted=int_fishz_avg_co_post, pal=cmap_diff[1:3], conds=['cond2','cond3'])
plt.xlim(-0.08, 0.1)
plt.ylim(0, 7.5)
plt.xticks([-0.08, -0.04, 0, 0.04, 0.08], ['-0.08', '-0.04', '0', '0.04', '0.08'])
plt.yticks([2, 4, 6], ['2', '4', '6'])
plt.savefig(join(fig_dir, 'fishz_condition_intelligibility_co.svg'), format='svg', dpi=1200)
plt.show()

#%% APPENDIX STUFF
stat_effort = get_sumstats('effort ~ condition + (1|subject_id)',
                              df_ce.query('condition == "cond2" | condition == "cond3"'))

stat_engagement = get_sumstats('engagement ~ condition + (1|subject_id)',
                              df_ce.query('condition == "cond2" | condition == "cond3"'))

#%% save stats...
save_dir = Path(Path.cwd()).expanduser() / 'data' / 'bayes_stats'
if not isdir(save_dir):
    makedirs(save_dir)
df_basic_avg_ce_post_tidy.to_pickle(join(save_dir, 'bayes_envelope_encoding_posteriors'))
df_basic_avg_co_post_tidy.to_pickle(join(save_dir, 'bayes_onsets_encoding_posteriors'))
df_fishz_avg_ce_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_envelope_encoding_posteriors'))
df_fishz_avg_co_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_onsets_encoding_posteriors'))

df_fishz_int_tidy.to_pickle(join(save_dir, 'bayes_fishz_intelligibility_ce_posteriors'))
df_fishz_int_tidy_co.to_pickle(join(save_dir, 'bayes_fishz_intelligibility_co_posteriors'))
df_fishz_eft_ce_tidy.to_pickle(join(save_dir, 'bayes_fishz_effort_ce_posteriors'))
df_fishz_eft_co_tidy.to_pickle(join(save_dir, 'bayes_fishz_effort_co_posteriors'))

# Appendix bayes plots...
df_fishz_horz_ce_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_envelope_horz_posteriors'))
df_fishz_horz_co_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_onsets_horz_posteriors'))
df_fishz_vert_ce_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_envelope_vert_posteriors'))
df_fishz_vert_co_post_tidy.to_pickle(join(save_dir, 'bayes_fishz_onsets_vert_posteriors'))
