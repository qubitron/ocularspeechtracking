# OcularSpeechTracking

## How to use this repo

Matlab code requires OBOB-OWNFT & mTRF-Toolbox:

- [ ] obob-ownft can be found [here](https://gitlab.com/obob/obob_ownft/-/wikis/home)

- [ ] mTRF-Toolbox can be found [here](https://github.com/mickcrosse/mTRF-Toolbox)


Python code requires to activate environment.yml file. We recommend the following steps:

```
mamba env create -p ./.venv
conda activate ./.venv
```       

## adjust the paths...

in order to run the code properly, adjust the Paths in the code to respective folders of OBOB-OWNFT & mTRF-Toolbox

code is commented at respective sections "adjust to where XY lives"

## order...

- [1] run all scripts with prefix 'a' (analysis)

- [2] run all scripts with prefix 's' (statistics)

- [3] run scripts with prefix 'p' (plots)