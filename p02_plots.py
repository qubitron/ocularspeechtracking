#%% imports...
from operator import rshift
import numpy as np
import pandas as pd
import eelbrain as eb
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mne
import fnmatch

from pymatreader import read_mat
from os import listdir, makedirs
from pathlib import Path
from os.path import join, isdir
from scipy.stats import zscore
from scipy.signal import resample

import matplotlib as mpl
new_rc_params = {'text.usetex': False,
"svg.fonttype": 'none'
}
mpl.rcParams.update(new_rc_params)

#%% plot params...
fig_dir = Path(Path.cwd()).expanduser() / 'data' / 'figures'
if not isdir(fig_dir):
    makedirs(fig_dir)
m_color = [0.8, 0.8, 0.8]
m_marker = 'o'
m_scale = 1.5
sns.set_style("ticks", rc={'xtick.direction': 'in', 'ytick.direction': 'in'})
sns.set_context("paper")#, rc={'font-family': 'Arial', "font.size":12, "axes.titlesize":14,"axes.labelsize":11})
cmap_diff = sns.color_palette("rocket",4)

#%% get correlating trials for envxgaze...
datadir = Path(Path.cwd()).expanduser() / 'data' / 'example_trials'
all_files = sorted(listdir(datadir))
all_subjects = np.unique([f[:12] for f in all_files]).tolist()

sub_data = read_mat(join(datadir,all_subjects[0]+'.mat'))
plt.plot(sub_data['max_xtime'], sub_data['max_xenv'], color=(0.3,0.3,0.3,0.5), linewidth=2.5)
plt.plot(sub_data['max_xtime'], sub_data['max_xgaze'], color='#559966', linewidth=2.5)
plt.savefig(join(fig_dir, 'S5_envxgazex.svg'), format='svg', dpi=1200)
plt.show()
plt.plot(sub_data['max_ytime'], sub_data['max_yenv'], color=(0.3,0.3,0.3,0.5), linewidth=2.5)
plt.plot(sub_data['max_ytime'], sub_data['max_ygaze'], color='#559966', linewidth=2.5)
plt.savefig(join(fig_dir, 'S5_envygazey.svg'), format='svg', dpi=1200)
plt.show()

sub_data = read_mat(join(datadir,all_subjects[1]+'.mat'))
plt.plot(sub_data['max_xtime'], sub_data['max_xenv'], color=(0.3,0.3,0.3,0.5), linewidth=2.5)
plt.plot(sub_data['max_xtime'], sub_data['max_xgaze'], color='#559966', linewidth=2.5)
plt.savefig(join(fig_dir, 'S27_envxgazex.svg'), format='svg', dpi=1200)
plt.show()
plt.plot(sub_data['max_ytime'], sub_data['max_yenv'], color=(0.3,0.3,0.3,0.5), linewidth=2.5)
plt.plot(sub_data['max_ytime'], sub_data['max_ygaze'], color='#559966', linewidth=2.5)
plt.savefig(join(fig_dir, 'S27_envxgazex.svg'), format='svg', dpi=1200)
plt.show()

#%% plot encoding results (bayes)
def plot_ridge(df, variable_name, values, labels, pal, plot_order=None, xlim=(-1,1), aspect=15, height=0.5):

      '''Pretty ridge plot function in python. This is based on code from https://seaborn.pydata.org/examples/kde_ridgeplot.html'''

      sns.set_theme(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})

      plotting_kwargs = {'row':variable_name, 
                         'hue': variable_name, 
                         'aspect': aspect,
                         'height': height,
                         'palette': pal}

      if plot_order is not None:
            plotting_kwargs.update({'row_order': plot_order,
                                    'hue_order': plot_order})
      
      g = sns.FacetGrid(df, **plotting_kwargs)

      g.map(sns.kdeplot, values,
            bw_adjust=.5, clip_on=False,
            fill=True, alpha=1, linewidth=1.5)
      g.map(sns.kdeplot, values, clip_on=False, color="w", lw=2, bw_adjust=.5)

      # passing color=None to refline() uses the hue mapping
      g.refline(y=0, linewidth=2, linestyle="-", color=None, clip_on=False)

      # Define and use a simple function to label the plot in axes coordinates
      def label(x, color, label):
            ax = plt.gca()
            ax.text(0, .3, label, fontweight="bold", color=color,
                  ha="left", va="center", transform=ax.transAxes)

      if labels:
        g.map(label, values)

      # Set the subplots to overlap
      g.figure.subplots_adjust(hspace=-.25)

      # Remove axes details that don't play well with overlap
      g.set_titles("")
      g.set(xlim=xlim)
      g.set(yticks=[], ylabel="")
      g.despine(bottom=True, left=True)

      return g

#%% fishz envelope...
bys_dir = Path(Path.cwd()).expanduser() / 'data' / 'bayes_stats'
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_envelope_encoding_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Envelope')
plt.savefig(join(fig_dir, 'fishz_envelope_encoding_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% fishz acoustic onsets...
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_onsets_encoding_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Acoustic Onsets')
plt.savefig(join(fig_dir, 'fishz_onsets_encoding_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% behavioral relevance...
# envelope intelligibility...
bys_dir = Path(Path.cwd()).expanduser() / 'data' / 'bayes_stats'
pal = sns.cubehelix_palette(4, rot=-.25, light=.7)
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_intelligibility_ce_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, xlim=[-50, 50], aspect=5, pal=pal[0:2], height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-50, -25, 0, 25, 50])
g.set_xlabels('Regression Coefficient')
plt.suptitle('Intelligibility')
plt.savefig(join(fig_dir, 'fishz_intelligibility_ce_dependent_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% envelope effort...
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_effort_ce_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, xlim=[-50,50], aspect=5, pal=pal[2:], height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-50, -25, 0, 25, 50])
g.set_xlabels('Regression Coefficient')
plt.suptitle('Effort')
plt.savefig(join(fig_dir, 'fishz_effort_ce_dependent_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% acoustic onsets intelligibility...
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_intelligibility_co_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, xlim=[-50,50], aspect=5, pal=pal[0:2], height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-50, -25, 0, 25, 50])
g.set_xlabels('Regression Coefficient')
plt.suptitle('Intelligibility')
plt.savefig(join(fig_dir, 'fishz_intelligibility_co_dependent_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% acoustic onsets effort...
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_effort_co_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, xlim=[-50,50], aspect=5, pal=pal[2:], height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-50, -25, 0, 25, 50])
g.set_xlabels('Regression Coefficient')
plt.suptitle('Effort')
plt.savefig(join(fig_dir, 'fishz_effort_co_dependent_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% Appendx bayes plots...
bys_dir = Path(Path.cwd()).expanduser() / 'data' / 'bayes_stats'
df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_envelope_horz_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Envelope Horizontal')
plt.savefig(join(fig_dir, 'fishz_envelope_horz_bayes.svg'), format='svg', dpi=1200)
plt.show()

df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_onsets_horz_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Acoustic Onsets Horizontal')
plt.savefig(join(fig_dir, 'fishz_onsets_horz_bayes.svg'), format='svg', dpi=1200)
plt.show()

df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_envelope_vert_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Envelope Vertical')
plt.savefig(join(fig_dir, 'fishz_envelope_vert_bayes.svg'), format='svg', dpi=1200)
plt.show()

df_plot = pd.read_pickle(join(bys_dir, 'bayes_fishz_onsets_vert_posteriors'))
g = plot_ridge(df_plot, 'variable', 'value', labels=True, pal=cmap_diff, aspect=5, xlim=(-0.04, .08), height=1)
g.refline(x=0, linewidth=2)
g.set(xticks=[-0.02, 0.00, 0.02, 0.04, 0.06])
g.set_xlabels("$\Delta$ Prediction Accuracy (z')")
plt.suptitle('Acoustic Onsets Vertical')
plt.savefig(join(fig_dir, 'fishz_onsets_vert_bayes.svg'), format='svg', dpi=1200)
plt.show()

#%% get encoding results...
enc_dir = Path(Path.cwd()).expanduser() / 'data' / 'mTRF' / 'eye' / 'fwd'
bhv_dir = Path(Path.cwd()).expanduser() / 'data' / 'behavioral'

all_files = sorted(listdir(join(enc_dir, 'c/')))
all_subjects = np.unique([f[:12] for f in all_files]).tolist()

# channel into separate cols (as dv)
def load_enc(fname,ch_idx,mname,cm):
    enc_data_test = read_mat(join(enc_dir, mname+fname))
    enc_data_c = read_mat(join(enc_dir, cm+fname))

    # get condition results...
    tmp = []
    for k in enc_data_test['data_sub'].keys():
        acc_test = enc_data_test['data_sub'][k]['stats']['r'][ch_idx]
        acc_cont = enc_data_c['data_sub'][k]['stats']['r'][ch_idx]
        acc_diff = acc_test - acc_cont
        tmp.append(acc_diff) #cond1-4
    return tmp

def load_mod(fname,ch_idx,mname,ft_idx):
    enc_data_test = read_mat(join(enc_dir, mname+fname))

    # get condition envelope models...
    tmp = []
    tmp_t = []
    for k in enc_data_test['data_sub'].keys():
        mod_env = enc_data_test['data_sub'][k]['model']['w'][ft_idx,:,ch_idx]
        t = enc_data_test['data_sub'][k]['model']['t']
        n_rs = 10*len(t)
        dat_rs = resample(mod_env, n_rs, t, window='hamming')
        tmp.append(dat_rs[0])
        tmp_t.append(dat_rs[1])
    return tmp, tmp_t

def load_bhv(fname, varname, *args):
    bhv_data = read_mat(join(bhv_dir, fname))['data_behav']
    tmp = []
    for k in bhv_data.keys():
        tmp.append(bhv_data[k][varname]) #cond1-3
    tmp.append(bhv_data['multispeaker'][varname]) # cond3 = cond4
    return tmp

def load_and_append(func, all_files, what, mname, cm):
    res = [func(fname,what,mname,cm) for fname in all_files]
    return np.concatenate(res,axis=0) #subj1-30

def load_and_append_mod(func, all_files, what, mname, ftname):
    out = [func(fname, what, mname, ftname) for fname in all_files]
    res, lags = zip(*out)
    return np.concatenate(res,axis=0), np.concatenate(lags,axis=0) #subj1-30

#%% get data...
enc_horizontal_ce = load_and_append(load_enc, all_files, 0, 'ce/', 'c/') #first channel
enc_vertical_ce = load_and_append(load_enc, all_files, 1, 'ce/', 'c/') #second channel
enc_avg_ce = (enc_horizontal_ce + enc_vertical_ce)/2 # average over both channels

enc_horizontal_co = load_and_append(load_enc, all_files, 0, 'co/', 'c/')
enc_vertical_co = load_and_append(load_enc, all_files, 1, 'co/', 'c/')
enc_avg_co = (enc_horizontal_co + enc_vertical_co)/2

intelligibility = load_and_append(load_bhv, all_files, 'intelligibility', None, None)
engagement = load_and_append(load_bhv, all_files, 'motivation', None, None)
effort = load_and_append(load_bhv, all_files, 'effort', None, None)  

#%% create df for enc
all_conds = ['ign_ss', 'att_ss', 'att_ms', 'ign_ms']
conditions = eb.combine([eb.Factor(all_conds)]*len(all_subjects))
subject_ids = eb.combine([eb.Factor(all_subjects, random=True, repeat=len(all_conds))])

df_ce = pd.DataFrame({'subject_id': subject_ids,
                   'condition': conditions,
                   'enc_horizontal': enc_horizontal_ce,
                   'enc_vertical' : enc_vertical_ce,
                   'enc_avg' : enc_avg_ce,
                   'intelligibility': intelligibility,
                   'engagement': engagement,
                   'effort': effort})

df_co = pd.DataFrame({'subject_id': subject_ids,
                   'condition': conditions,
                   'enc_horizontal': enc_horizontal_co,
                   'enc_vertical' : enc_vertical_co,
                   'enc_avg' : enc_avg_co,
                   'intelligibility': intelligibility,
                   'engagement': engagement,
                   'effort': effort})


#%% TRFs...
env_horizontal_ce, lags = load_and_append_mod(load_mod, all_files, 0, 'ce/', 2)
env_vertical_ce, lags = load_and_append_mod(load_mod, all_files, 1, 'ce/', 2)
aco_horizontal_co, lags = load_and_append_mod(load_mod, all_files, 0, 'co/', 2)
aco_vertical_co, lags = load_and_append_mod(load_mod, all_files, 1, 'co/', 2)

vis_horizontal_ce, lags = load_and_append_mod(load_mod, all_files, 0, 'ce/', 0)
vis_vertical_ce, lags = load_and_append_mod(load_mod, all_files, 1, 'ce/', 0)
ons_horizontal_ce, lags = load_and_append_mod(load_mod, all_files, 0, 'ce/', 1)
ons_vertical_ce, lags = load_and_append_mod(load_mod, all_files, 1, 'ce/', 1)

#%% create df for models...
sub_ids = eb.combine([eb.Factor(all_subjects, random=True, repeat=len(all_conds)*np.shape(lags)[1])])
conds = eb.combine([eb.Factor(all_conds, random=True, repeat=np.shape(lags)[1])]*len(all_subjects))

df_ce_env = pd.DataFrame({'subject_id': sub_ids,
                   'condition': conds,
                   'env_horizontal': env_horizontal_ce.flatten(),
                   'env_vertical': env_vertical_ce.flatten(),
                   'vis_horizontal': vis_horizontal_ce.flatten(),
                   'vis_vertical': vis_vertical_ce.flatten(),
                   'ons_horizontal': ons_horizontal_ce.flatten(),
                   'ons_vertical': ons_vertical_ce.flatten(),
                   'lags' : lags.flatten()
                   })

df_co_ons = pd.DataFrame({'subject_id': sub_ids,
                   'condition': conds,
                   'ons_horizontal': aco_horizontal_co.flatten(),
                   'ons_vertical': aco_vertical_co.flatten(),
                   'lags' : lags.flatten()
                   })

#%% plot params...
m_color = [0.8, 0.8, 0.8]
m_marker = 'o'
m_scale = 1.5
sns.set_style("ticks", rc={'xtick.direction': 'in', 'ytick.direction': 'in'})
sns.set_context("paper")#, rc={'font-family': 'Arial', "font.size":12, "axes.titlesize":14,"axes.labelsize":11})
cmap_diff = sns.color_palette("rocket",4)

#%% trf horizontal envelope
fig, ax = plt.subplots(figsize=(5,5))
sns.lineplot(data=df_ce_env, x='lags', y='env_horizontal', hue='condition',
            palette=cmap_diff, linewidth=3)

plt.axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
plt.xlabel('Lags (ms)')
plt.ylabel('TRF weights (a.u.)')
plt.suptitle('Envelope horizontal Eye-Movements')
sns.despine()
plt.xlim(-50, 500)
plt.ylim(-0.75, 1.75)
#plt.tight_layout()
plt.savefig(join(fig_dir, 'trf_horizontal_envelope.svg'), format='svg', dpi=1200)
plt.show()

#%% trf vertical envelope
fig, ax = plt.subplots(figsize=(5,5))
g1 = sns.lineplot(data=df_ce_env, x='lags', y='env_vertical', hue='condition',
            palette=cmap_diff, linewidth=3)

plt.axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
plt.xlabel('Lags (ms)')
plt.ylabel('TRF weights (a.u.)')
plt.suptitle('Envelope vertical Eye-Movements')
sns.despine()
plt.xlim(-50, 500)
plt.ylim(-0.75, 1.75)
#plt.tight_layout()
plt.savefig(join(fig_dir, 'trf_vertical_envelope.svg'), format='svg', dpi=1200)
plt.show()

#%% trf horizontal acoustic onsets
fig, ax = plt.subplots(figsize=(5,5))
g2 = sns.lineplot(data=df_co_ons, x='lags', y='ons_horizontal', hue='condition',
            palette=cmap_diff, linewidth=3)

plt.axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
plt.xlabel('Lags (ms)')
plt.ylabel('TRF weights (a.u.)')
plt.suptitle('Onsets horizontal Eye-Movements')
sns.despine()
plt.xlim(-50, 500)
plt.ylim(-0.75, 1.75)
#plt.tight_layout()
plt.savefig(join(fig_dir, 'trf_horizontal_onsets.svg'), format='svg', dpi=1200)
plt.show()

#%% trf vertical acoustic onsets
fig, ax = plt.subplots(figsize=(5,5))
g3 = sns.lineplot(data=df_co_ons, x='lags', y='ons_vertical', hue='condition',
            palette=cmap_diff, linewidth=3)

plt.axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
plt.xlabel('Lags (ms)')
plt.ylabel('TRF weights (a.u.)')
plt.suptitle('Onsets vertical Eye-Movements')
sns.despine()
plt.xlim(-50, 500)
plt.ylim(-0.75, 1.75)
#plt.tight_layout()
plt.savefig(join(fig_dir, 'trf_vertical_onsets.svg'), format='svg', dpi=1200)
plt.show()

#%% plot max MEG trfs...
algnd_dir = Path(Path.cwd()).expanduser() / 'data' / 'aligned' / 'eye'
all_files = sorted(listdir(algnd_dir))
all_subjects = np.unique([f[:12] for f in all_files]).tolist()

floc = Path(Path.cwd()).expanduser() / 'data' / 'mTRF' /'meg_eye' / 'max_trfs' 
fname = 'max_trfs.mat'
all_dat = read_mat(join(floc, fname))
trf_dat_e = all_dat['max_trf_e']
trf_dat_ei = all_dat['max_trf_ei']
trf_dat_diff = all_dat['max_trf_diff']
all_conds = ['ass', 'ams', 'ums']
dat_e = []
dat_ei = []
dat_diff = []
lag = []
for i,j in enumerate(all_subjects):
    for c in all_conds:
        mod_env_e = trf_dat_e[c][i,:]
        mod_env_ei = trf_dat_ei[c][i,:]
        mod_env_diff = trf_dat_diff[c][i,:]
        t = all_dat['lags']
        n_rs = 10*len(t)
        dat_rs_e = resample(mod_env_e, n_rs, t, window='hamming')
        dat_rs_ei = resample(mod_env_ei, n_rs, t, window='hamming')
        dat_rs_diff = resample(mod_env_diff, n_rs, t, window='hamming')
        dat_e.append(dat_rs_e[0])
        dat_ei.append(dat_rs_ei[0])
        dat_diff.append(dat_rs_diff[0])
        lag.append(dat_rs_e[1])
    
sub_ids = eb.combine([eb.Factor(all_subjects, random=True, repeat=len(all_conds)*np.shape(dat_rs_e[1])[0])])
conds = eb.combine([eb.Factor(all_conds, random=True, repeat=np.shape(dat_rs_e[1]))]*len(all_subjects))
lags = all_dat['lags']*(len(all_conds*len(all_subjects)))
df_trf = pd.DataFrame({'subject_id': sub_ids,
                   'condition': conds,
                   'trf_e': np.asarray(dat_e).flatten(),
                   'trf_ei': np.asarray(dat_ei).flatten(),
                   'trf_diff': np.asarray(dat_diff).flatten(),
                   'lags' : np.asarray(lag).flatten()
                   })

# plot params...
m_color = [0.8, 0.8, 0.8]
m_marker = 'o'
m_scale = 1.5
sns.set_style("ticks", rc={'xtick.direction': 'in', 'ytick.direction': 'in'})
sns.set_context("paper")#, rc={'font-family': 'Arial', "font.size":12, "axes.titlesize":14,"axes.labelsize":11})
cmap_diff = sns.color_palette("rocket",4)

# meg trfs...
m_grey = [0.3, 0.3, 0.3]
m_blue = [0.5, 0.8, 0.9]

# make figure...
fig, axes = plt.subplots(2,3, figsize=(15,5), gridspec_kw={'height_ratios': [1.5, 2]}, sharey='row')
axes[0, 0].set_ylabel('TRF weights (arb. unit)')
axes[1, 0].set_ylabel("Difference (c - c')")

for i, c in enumerate(all_conds):
    g1 = sns.lineplot(data=df_trf.query('condition == "{}"'.format(c)), x='lags', y='trf_e',
                color=m_grey, linewidth=3, ax=axes[0,i], label='c')
    g2 = sns.lineplot(data=df_trf.query('condition == "{}"'.format(c)), x='lags', y='trf_ei',
                color=m_blue, linewidth=3, ax=axes[0,i], label="c'")
    g3 = sns.lineplot(data=df_trf.query('condition == "{}"'.format(c)), x='lags', y='trf_diff',
                color=cmap_diff[i+1], linewidth=3, ax=axes[1,i])
    # Add horizontal and vertical lines
    axes[0, i].axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
    axes[0, i].axvline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
    axes[1, i].axhline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
    axes[1, i].axvline(0, color=[0.8, 0.8, 0.8], linestyle='dashed', zorder=0)
    axes[0, i].xaxis.set_visible(False)
    axes[0, i].spines['bottom'].set_visible(False)

    axes[0, i].set_xlim(-40, 500) # time of clusterstat...
    axes[1, i].set_xlim(-40, 500) # time of clusterstat...
    axes[0, i].set_ylim(-0.005, 0.025)
    axes[1, i].set_ylim(-0.0001, 0.00175)

    sns.despine(ax=axes[0, i], bottom=True)
    sns.despine(ax=axes[1, i])
    
plt.legend()
plt.tight_layout()
plt.savefig(join(fig_dir, 'meg_trf_mediation.svg'), format='svg', dpi=1200)
plt.show()
