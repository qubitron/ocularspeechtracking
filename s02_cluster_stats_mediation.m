%%
clear all
close all

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft') % adjust path to where obob_ownft lives...
obob_init_ft; % Initialize obob_ownft

%% set vars...
addpath('./helpers/jschubert/myfuns')
% colormap
ft_hastoolbox('brewermap',1); % get colormap
addpath('./helpers/brewermap/');

fig_dir = './data/figures/';
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir)
end %if

%% get mag labels from example vp...
load('./data/aligned/meg_eye/vp1.mat', 'data_all')
cfg = [];
cfg.channel = 'MEGMAG';
data = ft_selectdata(cfg,data);
mag_labels = data.label;

%% load data
datadir = './data/boosting_mediation/';

all_subjects = js_getsubjectsfrom(datadir);
for subi = 1:length(all_subjects)
    subject = all_subjects{subi};
    load(fullfile(datadir, [subject,'.mat']))
    
    conds = fieldnames(fit_simple);
    for cond = 1:length(conds)
        trf_plain = fit_simple.(conds{cond}).trf;
        direct_trf_x = fit_mediation_x.(conds{cond}).trf;
        direct_trf_y = fit_mediation_y.(conds{cond}).trf;
        ctrl_trf_x = fit_control_x.(conds{cond}).trf;
        ctrl_trf_y = fit_control_y.(conds{cond}).trf;
        enc = fit_simple.(conds{cond}).r_rank;
        
        both = [];
        both(1,:,:) = direct_trf_x;
        both(2,:,:) = direct_trf_y;
        both_control = [];
        both_control(1,:,:) = ctrl_trf_x;
        both_control(2,:,:) = ctrl_trf_y;
    
        trf_direct = squeeze(mean(both));
        trf_control = squeeze(mean(both_control));
        trf_diff = abs(trf_plain) - abs(trf_direct);
        trf_diff_ctrl = abs(trf_control) - abs(trf_direct);
        
        time = fit_simple.(conds{cond}).time;
        
        %% fake fieldtrip struct...
        trf_plain_ft = [];
        trf_plain_ft.dimord = 'chan_time';
        trf_plain_ft.fsample = 50;
        trf_plain_ft.label = mag_labels;
        trf_plain_ft.time = time;
        
        trf_direct_ft = trf_plain_ft;
        trf_control_ft = trf_plain_ft;
        trf_diff_ft = trf_plain_ft;
        trf_diff_ctrl_ft = trf_plain_ft;
        
        enc_ft = trf_plain_ft;
        enc_ft.time = 1;
        enc_ft.avg = enc;
        
        trf_plain_ft.avg = trf_plain;
        trf_direct_ft.avg = trf_direct;
        trf_control_ft.avg = trf_control;
        trf_diff_ft.avg = trf_diff;
        trf_diff_ctrl_ft.avg = trf_diff_ctrl;
        
        trf_plain_all.(conds{cond}){subi} = trf_plain_ft;
        trf_direct_all.(conds{cond}){subi} = trf_direct_ft;
        trf_control_all.(conds{cond}){subi} = trf_control_ft;
        trf_diff_all.(conds{cond}){subi} = trf_diff_ft;
        trf_diff_ctrl_all.(conds{cond}){subi} = trf_diff_ctrl_ft;
        enc_all.(conds{cond}){subi} = enc_ft;
             
    end %for
    
end

%% check ga...
cfg = [];
ga_att_ss_plain = ft_timelockgrandaverage(cfg, trf_plain_all.ass{:});
ga_att_ms_plain = ft_timelockgrandaverage(cfg, trf_plain_all.ams{:});
ga_ign_ms_plain = ft_timelockgrandaverage(cfg, trf_plain_all.ums{:});
ga_att_ss_direct = ft_timelockgrandaverage(cfg, trf_direct_all.ass{:});
ga_att_ms_direct = ft_timelockgrandaverage(cfg, trf_direct_all.ams{:});
ga_ign_ms_direct = ft_timelockgrandaverage(cfg, trf_direct_all.ums{:});
ga_att_ss_control = ft_timelockgrandaverage(cfg, trf_control_all.ass{:});
ga_att_ms_control = ft_timelockgrandaverage(cfg, trf_control_all.ams{:});
ga_ign_ms_control = ft_timelockgrandaverage(cfg, trf_control_all.ums{:});
ga_att_ss_diff = ft_timelockgrandaverage(cfg, trf_diff_all.ass{:});
ga_att_ms_diff = ft_timelockgrandaverage(cfg, trf_diff_all.ams{:});
ga_ign_ms_diff = ft_timelockgrandaverage(cfg, trf_diff_all.ums{:});
ga_att_ss_diff_control = ft_timelockgrandaverage(cfg, trf_diff_ctrl_all.ass{:});
ga_att_ms_diff_control = ft_timelockgrandaverage(cfg, trf_diff_ctrl_all.ams{:});
ga_ign_ms_diff_control = ft_timelockgrandaverage(cfg, trf_diff_ctrl_all.ums{:});

ga_enc_att_ss = ft_timelockgrandaverage(cfg, enc_all.ass{:});
ga_enc_att_ms = ft_timelockgrandaverage(cfg, enc_all.ams{:});
ga_enc_ign_ms = ft_timelockgrandaverage(cfg, enc_all.ums{:});

%% get max. channel based on single speaker...
[max_r, max_chan] = max(ga_enc_att_ss.avg);

%% get max. chan trfs..
max_trf_e = [];
max_trf_ei = [];
for isub = 1:length(all_subjects)
    for cond = 1:length(conds)
        max_trf_e.(conds{cond})(isub,:) = trf_plain_all.(conds{cond}){isub}.avg(max_chan,:);
        max_trf_ei.(conds{cond})(isub,:) = trf_direct_all.(conds{cond}){isub}.avg(max_chan,:);
        max_trf_diff.(conds{cond})(isub,:) = trf_diff_all.(conds{cond}){isub}.avg(max_chan,:);
    end %for isub
end %for ifn

lags = time*1000; % s to ms
outdir_max = './data/mTRF/meg_eye/max_trfs/';
if ~exist(outdir_max, 'dir')
    mkdir(outdir_max)
end %if
save(fullfile(outdir_max, 'max_trfs.mat'), 'max_trf_e', 'max_trf_ei', 'max_trf_diff', 'lags')

%% control plot neural envelope encoding...
t_plot = fit_simple.ass.time;
figure;
cfg = [];
cfg.parameter = 'avg';
cfg.layout = '/mnt/obob/staff/jschubert/helpers/templates/neuromag306mag_helmet.mat';
cfg.comment = 'no';
cfg.figure = 'gcf';
cfg.zlim = [0, 0.15];
cfg.colorbar = 'SouthOutside';
cfg.colormap = colormap(brewermap(64, '*RdBu'));
subplot(1,3,1)
cfg.title = 'ga envelope encoding att ss';
ft_topoplotER(cfg, ga_enc_att_ss)
subplot(1,3,2)
cfg.title = 'ga envelope encoding att ms';
ft_topoplotER(cfg, ga_enc_att_ms)
subplot(1,3,3)
cfg.title = 'ga envelope encoding ign ms';
ft_topoplotER(cfg, ga_enc_ign_ms)
saveas(gcf, fullfile(fig_dir, 'topoplots_ga_neural_encoding_envelope.svg'))

% here we resample for illustration urposes only...
t_resampled = linspace(t_plot(1), t_plot(end), length(t_plot) * (500 / 50));
% Preallocate the resampled data matrix
att_ss_resampled = zeros(length(t_resampled), 102);
att_ms_resampled = zeros(length(t_resampled), 102);
ign_ms_resampled = zeros(length(t_resampled), 102);

% Resample each channel
for i = 1:102
    att_ss_resampled(:, i) = smoothdata(resample(ga_att_ss_plain.avg(i,:), 500, 50)', 'gaussian', 50);
    att_ms_resampled(:, i) = smoothdata(resample(ga_att_ms_plain.avg(i,:), 500, 50)', 'gaussian', 50);
    ign_ms_resampled(:, i) = smoothdata(resample(ga_ign_ms_plain.avg(i,:), 500, 50)', 'gaussian', 50);
end

figure;
set(gcf, 'Name', 'matplotlib')
rgb = ft_colormap('twilight');
plot(t_resampled,att_ss_resampled, 'LineWidth', 1.5);
colororder(colormap(rgb))
xlim([-0.06, 0.5])
ylim([-0.02 0.02])

figure;
set(gcf, 'Name', 'matplotlib')
rgb = ft_colormap('twilight');
plot(t_resampled,att_ms_resampled, 'LineWidth', 1.5);
colororder(colormap(rgb))
xlim([-0.06, 0.5])
ylim([-0.02 0.02])

figure;
set(gcf, 'Name', 'matplotlib')
rgb = ft_colormap('twilight');
plot(t_resampled,ign_ms_resampled, 'LineWidth', 1.5);
colororder(colormap(rgb))
xlim([-0.06, 0.5])
ylim([-0.02 0.02])
saveas(gcf, fullfile(fig_dir, 'trfs_ga_neural_encoding_envelope.svg'))

%% cluster stats...
cfg = [];
cfg.latency = [-0.04 0.5]; % due to 50Hz fs, this can only be -0.04. or -0.06 (if we want to be sure about edge artefacts, we choose -0.04)
cfg.avgovertime = 'yes';
cfg.method           = 'montecarlo';
cfg.correctm         = 'cluster';
cfg.statistic        = 'depsamplesT';
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.tail             = -1;
cfg.clustertail      = -1;
cfg.alpha            = 0.05;
cfg.numrandomization = 10000;
cfg.minnbchan        = 2;
cfg_neighb.method    = 'template';%'template';
cfg_neighb.template  = 'neuromag306mag_neighb';
cfg.neighbours       = ft_prepare_neighbours(cfg_neighb, trf_direct_all.ass{1});

% prepare design
n = size(trf_direct_all.ass,2);
design = zeros(2,n*2);
design(1,:) = repmat(1:n,1,2);
design(2,:) = repelem(1:2,length(design)/2);

cfg.design   = design;
cfg.uvar     = 1;
cfg.ivar     = 2;

c_stats_att_ss = ft_timelockstatistics(cfg, trf_direct_all.ass{:}, trf_plain_all.ass{:});
c_stats_att_ms = ft_timelockstatistics(cfg, trf_direct_all.ams{:}, trf_plain_all.ams{:});
c_stats_ign_ms = ft_timelockstatistics(cfg, trf_direct_all.ums{:}, trf_plain_all.ums{:});

c_stats_control_att_ss = ft_timelockstatistics(cfg, trf_direct_all.ass{:}, trf_control_all.ass{:});
c_stats_control_att_ms = ft_timelockstatistics(cfg, trf_direct_all.ams{:}, trf_control_all.ams{:});
c_stats_control_ign_ms = ft_timelockstatistics(cfg, trf_direct_all.ums{:}, trf_control_all.ums{:});

%% get t-vals...
avg_att_ss = mean(c_stats_att_ss.stat(c_stats_att_ss.mask));
avg_att_ms = mean(c_stats_att_ms.stat(c_stats_att_ms.mask));
avg_ign_ms = mean(c_stats_ign_ms.stat(c_stats_ign_ms.mask));

avg_control_att_ss = mean(c_stats_control_att_ss.stat(c_stats_control_att_ss.mask));
avg_control_att_ms = mean(c_stats_control_att_ms.stat(c_stats_control_att_ms.mask));
avg_control_ign_ms = mean(c_stats_control_ign_ms.stat(c_stats_control_ign_ms.mask));

%% plot it...
addpath('./helpers/brewermap/');
all_colors = [0.63139686 0.10067417 0.35664819; ...
    0.90848638 0.24568473 0.24598324; ...
    0.96298491 0.6126247 0.45145074];

grey = [.3 .3 .3];
cmap_att_ss = customcolormap([0 0.5 1], [grey; 1 1 1; all_colors(1,:)]);
cmap_att_ms = customcolormap([0 0.5 1], [grey; 1 1 1; all_colors(2,:)]);
cmap_ign_ms = customcolormap([0 0.5 1], [grey; 1 1 1; all_colors(3,:)]);

cfg = [];
cfg.parameter = 'stat';
cfg.colormap = colormap(brewermap(64, '*RdBu'));
cfg.zlim = 'maxabs';
cfg.colorbartext = 'T-value';
cfg.gridscale = 400;
cfg.style = 'both_imsat';
cfg.marker = 'off';
cfg.highlight = 'on';
cfg.highlightsymbol  = {'.'};
cfg.highlightcolor   = [.2 .2 .2];
cfg.highlightsize    = 50;
cfg.layout = 'neuromag306mag_helmet';
cfg.highlightchannel = {c_stats_att_ss.label(c_stats_att_ss.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ss);

ft_topoplotER(cfg, c_stats_att_ss);
title('clusterstat att ss (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_att_ss.svg'))

cfg.highlightchannel = {c_stats_att_ms.label(c_stats_att_ms.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ms);
ft_topoplotER(cfg, c_stats_att_ms);
title('clusterstat att ms (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_att_ms.svg'))

cfg.highlightchannel = {c_stats_ign_ms.label(c_stats_ign_ms.mask)};
cfg.colormap = colormap(cmap_ign_ms);
cfg.colorbar = 'yes';
ft_topoplotER(cfg, c_stats_ign_ms);
title('clusterstat ign ms (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_ign_ms.svg'))

cfg.highlightchannel = {c_stats_control_att_ss.label(c_stats_control_att_ss.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ss);
ft_topoplotER(cfg, c_stats_control_att_ss);
title('clusterstat att ss (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_att_ss.svg'))

cfg.highlightchannel = {c_stats_control_att_ms.label(c_stats_control_att_ms.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ms);
ft_topoplotER(cfg, c_stats_control_att_ms);
title('clusterstat att ms (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_att_ms.svg'))

cfg.highlightchannel = {c_stats_control_ign_ms.label(c_stats_control_ign_ms.mask)};
cfg.colormap = colormap(cmap_ign_ms);
cfg.colorbar = 'yes';
ft_topoplotER(cfg, c_stats_control_ign_ms);
title('clusterstat ign ms (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_ign_ms.svg'))

%% calculate cohen's d...
subj = numel(all_subjects);
cohensd_att_ss = (c_stats_att_ss.stat)./sqrt(subj);
cohensd_att_ms = (c_stats_att_ms.stat)./sqrt(subj);
cohensd_ign_ms = (c_stats_ign_ms.stat)./sqrt(subj);

cohensd_control_att_ss = (c_stats_control_att_ss.stat)./sqrt(subj);
cohensd_control_att_ms = (c_stats_control_att_ms.stat)./sqrt(subj);
cohensd_control_ign_ms = (c_stats_control_ign_ms.stat)./sqrt(subj);

%% append to struct...
c_stats_att_ss.stat = cohensd_att_ss;
c_stats_att_ms.stat = cohensd_att_ms;
c_stats_ign_ms.stat = cohensd_ign_ms;

c_stats_control_att_ss.stat = cohensd_control_att_ss;
c_stats_control_att_ms.stat = cohensd_control_att_ms;
c_stats_control_ign_ms.stat = cohensd_control_ign_ms;

avg_att_ss_cd = mean(c_stats_att_ss.stat(c_stats_att_ss.mask));
avg_att_ms_cd = mean(c_stats_att_ms.stat(c_stats_att_ms.mask));
avg_ign_ms_cd = mean(c_stats_ign_ms.stat(c_stats_ign_ms.mask));

avg_control_att_ss_cd = mean(c_stats_control_att_ss.stat(c_stats_control_att_ss.mask));
avg_control_att_ms_cd = mean(c_stats_control_att_ms.stat(c_stats_control_att_ms.mask));
avg_control_ign_ms_cd = mean(c_stats_control_ign_ms.stat(c_stats_control_ign_ms.mask));

%% plot with cohen's d
cfg = [];
cfg.parameter = 'stat';
cfg.colormap = colormap(brewermap(64, '*RdBu'));
cfg.zlim = 'maxabs';
cfg.colorbartext = 'Cohens d';
cfg.gridscale = 400;
cfg.style = 'both_imsat';
cfg.marker = 'off';
cfg.highlight = 'on';
cfg.highlightsymbol  = {'.'};
cfg.highlightcolor   = [.2 .2 .2];
cfg.highlightsize    = 50;
cfg.layout = 'neuromag306mag_helmet';
cfg.highlightchannel = {c_stats_att_ss.label(c_stats_att_ss.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ss);

ft_topoplotER(cfg, c_stats_att_ss);
title('clusterstat att ss (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_att_ss_cohensd.svg'))

cfg.highlightchannel = {c_stats_att_ms.label(c_stats_att_ms.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ms);
ft_topoplotER(cfg, c_stats_att_ms);
title('clusterstat att ms (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_att_ms_cohensd.svg'))

cfg.highlightchannel = {c_stats_ign_ms.label(c_stats_ign_ms.mask)};
cfg.colormap = colormap(cmap_ign_ms);
cfg.colorbar = 'yes';
ft_topoplotER(cfg, c_stats_ign_ms);
title('clusterstat ign ms (direct < plain)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_ign_ms_cohensd.svg'))

cfg.highlightchannel = {c_stats_control_att_ss.label(c_stats_control_att_ss.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ss);
ft_topoplotER(cfg, c_stats_control_att_ss);
title('clusterstat att ss (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_att_ss_cohensd.svg'))

cfg.highlightchannel = {c_stats_control_att_ms.label(c_stats_control_att_ms.mask)};
cfg.colorbar = 'yes';
cfg.colormap = colormap(cmap_att_ms);
ft_topoplotER(cfg, c_stats_control_att_ms);
title('clusterstat att ms (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_att_ms_cohensd.svg'))

cfg.highlightchannel = {c_stats_control_ign_ms.label(c_stats_control_ign_ms.mask)};
cfg.colormap = colormap(cmap_ign_ms);
cfg.colorbar = 'yes';
ft_topoplotER(cfg, c_stats_control_ign_ms);
title('clusterstat ign ms (direct < control)')
saveas(gcf, fullfile(fig_dir, 'topoplot_cstats_control_ign_ms_cohensd.svg'))

