%% clear...
clear all
close all

%% init obob ownft...
addpath('/mnt/obob/obob_ownft'); %change to where obob_ownft lives...
obob_init_ft; % Initialize obob_ownft

addpath(genpath('./helpers/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light

%% set vars...
addpath('./helpers/jschubert/myfuns');
addpath('./helpers/brewermap');
data_dir = './data/aligned/meg_eye/';
all_subjects = js_getsubjectsfrom(data_dir);

all_conds = {'vis'; 'ss'; 'ms'};
all_colors = [0.29977678 0.11356089 0.29254823;...
    0.63139686 0.10067417 0.35664819; ...
    0.90848638 0.24568473 0.24598324];
cmap_vis = customcolormap([0 0.5 1], [0 0 0; all_colors(1,:); 1 1 1]);
cmap_sin = customcolormap([0 0.5 1], [0 0 0; all_colors(2,:); 1 1 1]);
cmap_mul = customcolormap([0 0.5 1], [0 0 0; all_colors(3,:); 1 1 1]);
cmap_vis_ss = customcolormap([0 0.5 1], [all_colors(2,:); 1 1 1; all_colors(1,:)]);
cmap_vis_ms = customcolormap([0 0.5 1], [all_colors(3,:); 1 1 1; all_colors(1,:)]);

fig_dir = './data/figures/';
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir)
end %if

%% set screen dims...
% Default Trackpixx setup...
distance = 82;
screen_width = 61; %in cm
screen_rect = [0 0 1920 1080];
va1_deg_cm = 2*pi*distance/360; % visual angle 1 deg [unit:cm]
px_in_cm = screen_width/screen_rect(3);
va1_deg_px = floor(va1_deg_cm/px_in_cm);
px2deg = 1/va1_deg_px;

screen_rect_deg = screen_rect * px2deg;
gabor_deg = 800*px2deg;

%%
for subj=1:length(all_subjects)
%     keep gazeall subj all_subjects data_dir
    load(fullfile(data_dir, [all_subjects{subj} '.mat']), 'data_all')
    
    cfg = [];
    cfg.channel =     {'x','y'};
    et= ft_selectdata(cfg,data_all);
    %%
    for trl=1:length(et.trial)
        close all
        %     tmp=horzcat(et6l.trial{:});
        tmp = et.trial{trl};
        
        x=tmp(1,:);
        y=tmp(2,:);
        figure;
        subplot(2,2,1);scatter(x,y,'.')
        xlim([-screen_rect_deg(3) screen_rect_deg(3)]);
        ylim([-screen_rect_deg(4) screen_rect_deg(4)]);
        set(gca,'Color','none');
        %%
        % Bin the data:
        ptsx = linspace(-screen_rect_deg(3), screen_rect_deg(3), 101);
        ptsy = linspace(-screen_rect_deg(4), screen_rect_deg(4), 101);
        N = histcounts2(y(:), x(:), ptsy, ptsx);
        %  Create Gaussian filter matrix:
        [xG, yG] = meshgrid(-screen_rect_deg(3):screen_rect_deg(3),-screen_rect_deg(4):screen_rect_deg(4));
        sigma =5.5;
        g = exp(-xG.^2./(2.*sigma.^2)-yG.^2./(2.*sigma.^2));
        g = g./sum(g(:));
        % Plot heatmap:
        subplot(2, 2, 2);
        imagesc(ptsx, ptsy, conv2(N, g, 'same'));
        axis equal;
        set(gca, 'XLim', ptsx([1 end]), 'YLim', ptsy([1 end]), 'YDir', 'normal');
        % caxis([0 4])
        xlim([-screen_rect_deg(3) screen_rect_deg(3)]);
        ylim([-screen_rect_deg(4) screen_rect_deg(4)]);
        %%
        tmp=conv2(N, g, 'same');
%         freqpre.freq=pts(2:end);
        freqpre.powspctrm=tmp;
        pow=zeros(1,numel(freqpre.powspctrm(:,1)),numel(freqpre.powspctrm(1,:)));
        pow(1,:,:)=freqpre.powspctrm;
        
        allgaze(trl,:,:,:)=pow;
    end
    %%
    gaze.powspctrm=allgaze;
    gaze.time=ptsx(2:end);
    gaze.freq=ptsy(2:end);
    gaze.label={'gaze'};
    gaze.dimord = 'rpt_chan_freq_time';
    gaze.trialinfo=et.trialinfo;
    
    gazeall{subj}=gaze;
end

%%
for subj=1:length(all_subjects)
    %trialinfo ist 1st column die conditions: 1 = visual, 2=single speaker, 3=multispeaker (4=3...daten sind die gleichen, da sind nur die aligned envelopes natürlich anders)..sprich für heatmaps relevant nur trialinfo(:,1) = [1, 2 ,3]
    indvisual=find(gazeall{subj}.trialinfo(:,1)==1);
    indsinglespeaker=find(gazeall{subj}.trialinfo(:,1)==2);
    indmultispeaker=find(gazeall{subj}.trialinfo(:,1)==3);
    cfg = [];
    cfg.trials=indvisual;
    gazevis{subj}=ft_freqdescriptives(cfg,gazeall{subj});
    cfg.trials=indsinglespeaker;
    gazesingle{subj}=ft_freqdescriptives(cfg,gazeall{subj});
    cfg.trials=indmultispeaker;
    gazemulti{subj}=ft_freqdescriptives(cfg,gazeall{subj});
    %%
    maxval=max(reshape(gazevis{subj}.powspctrm,[1,size(gazevis{subj}.powspctrm,2)*size(gazevis{subj}.powspctrm,3)]));
    minval=min(reshape(gazevis{subj}.powspctrm,[1,size(gazevis{subj}.powspctrm,2)*size(gazevis{subj}.powspctrm,3)]));
    gazevis{subj}.powspctrm=(gazevis{subj}.powspctrm-minval)./(maxval-minval);
    
    maxval=max(reshape(gazesingle{subj}.powspctrm,[1,size(gazesingle{subj}.powspctrm,2)*size(gazesingle{subj}.powspctrm,3)]));
    minval=min(reshape(gazesingle{subj}.powspctrm,[1,size(gazesingle{subj}.powspctrm,2)*size(gazesingle{subj}.powspctrm,3)]));
    gazesingle{subj}.powspctrm=(gazesingle{subj}.powspctrm-minval)./(maxval-minval);
    maxval=max(reshape(gazemulti{subj}.powspctrm,[1,size(gazemulti{subj}.powspctrm,2)*size(gazemulti{subj}.powspctrm,3)]));
    minval=min(reshape(gazemulti{subj}.powspctrm,[1,size(gazemulti{subj}.powspctrm,2)*size(gazemulti{subj}.powspctrm,3)]));
    gazemulti{subj}.powspctrm=(gazemulti{subj}.powspctrm-minval)./(maxval-minval);
end
%%
gavis= ft_freqgrandaverage([],gazevis{:});
gasin= ft_freqgrandaverage([],gazesingle{:});
gamul = ft_freqgrandaverage([],gazemulti{:});

%%
cfg = [];
cfg.spmversion = 'spm12';
cfg.method           = 'montecarlo';
cfg.statistic        = 'ft_statfun_depsamplesT';
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';

cfg.tail             = 0;
cfg.clustertail      = 0;
cfg.alpha            = 0.05;
cfg.numrandomization = 10000;

cfg.neighbours=[];
subj = numel(all_subjects);
design = zeros(2,2*subj);
for i = 1:subj
    design(1,i) = i;
end
for i = 1:subj
    design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design   = design;
cfg.uvar     = 1;
cfg.ivar     = 2;

[stat] = ft_freqstatistics(cfg, gazesingle{:},gazevis{:});

%% calcuate stats for paper...
stat.stat(stat.mask==0)=0;% mask out all non significant
stat_vis_vs_single=stat;
t_pos = mean(squeeze(stat.stat(stat.posclusterslabelmat==1)));
t_neg = mean(squeeze(stat.stat(stat.negclusterslabelmat==1)));
ci = stat.posclusters.cirange;
cohensd=(stat_vis_vs_single.stat)./sqrt(subj);
stat_vis_vs_single.stat=cohensd;

% 
coh_d_pos = mean(squeeze(stat_vis_vs_single.stat(stat_vis_vs_single.posclusterslabelmat==1)));
p_pos = stat_vis_vs_single.posclusters.prob;
ci_pos = stat_vis_vs_single.cirange;
coh_d_neg = mean(squeeze(stat_vis_vs_single.stat(stat_vis_vs_single.negclusterslabelmat==1)));
p_neg = stat_vis_vs_single.negclusters.prob;
ci_neg = stat_vis_vs_single;

%%
[stat] = ft_freqstatistics(cfg, gazemulti{:},gazevis{:});
stat.stat(stat.mask==0)=0;% mask out all non significant
stat.cirange(stat.mask==0)=0;
t_pos = mean(squeeze(stat.stat(stat.posclusterslabelmat==1)));
t_neg = mean(squeeze(stat.stat(stat.negclusterslabelmat==1)));
stat_vis_vs_multi=stat;
cohensd=(stat_vis_vs_multi.stat)./sqrt(subj);
stat_vis_vs_multi.stat=cohensd;
coh_d_pos = mean(squeeze(stat_vis_vs_multi.stat(stat_vis_vs_multi.posclusterslabelmat==1)));
p_pos = stat_vis_vs_multi.posclusters.prob;
coh_d_neg = mean(squeeze(stat_vis_vs_multi.stat(stat_vis_vs_multi.negclusterslabelmat==1)));
p_neg = stat_vis_vs_multi.negclusters.prob;

%% plot gaze...
addpath('./helpers/brewermap/');

%%
figure;
cfg = [];
cfg.colormap = cmap_vis;
call_js_private('mv_plot_2D', cfg, squeeze(gavis.powspctrm));
xticks([1 50 100])
xticklabels({num2str(gavis.time(1)),'0', num2str(gavis.time(end))});
yticks([1 50 100])
yticklabels({num2str(round(gavis.freq(1))),'0', num2str(round(gavis.freq(end)))});
saveas(gcf, fullfile(fig_dir, 'gaze_vis.svg'))


figure;
cfg = [];
cfg.colormap = cmap_sin;
call_js_private('mv_plot_2D', cfg, squeeze(gasin.powspctrm));
xticks([1 50 100])
xticklabels({num2str(gasin.time(1)),'0', num2str(gasin.time(end))});
yticks([1 50 100])
yticklabels({num2str(round(gasin.freq(1))),'0', num2str(round(gasin.freq(end)))});
saveas(gcf, fullfile(fig_dir, 'gaze_ss.svg'))

figure;
cfg = [];
cfg.colormap = cmap_mul;
call_js_private('mv_plot_2D', cfg, squeeze(gamul.powspctrm));
xticks([1 50 100])
xticklabels({num2str(gamul.time(1)),'0', num2str(gamul.time(end))});
yticks([1 50 100])
yticklabels({num2str(round(gamul.freq(1))),'0', num2str(round(gamul.freq(end)))});
saveas(gcf, fullfile(fig_dir, 'gaze_ms.svg'))

%% clusters...
cfg = [];
cfg.avgoverchan = 'yes';
cfg.frequency = [-screen_rect_deg(4) screen_rect_deg(4)];
cfg.latency   = [-screen_rect_deg(3) screen_rect_deg(3)];
freq = ft_selectdata(cfg,stat_vis_vs_single);
meanpow = squeeze(mean(freq.stat, 1));
meanmask = squeeze(mean(freq.mask, 1));
% The finer time and frequency axes:
tim_interp = linspace(freq.time(1), freq.time(end), 512);
freq_interp = linspace(-screen_rect_deg(4), screen_rect_deg(4), 512);
% mask_interp = linspace(-10, 10, 512);
% We need to make a full time/frequency grid of both the original and
% interpolated coordinates. Matlab's meshgrid() does this for us:
[tim_grid_orig, freq_grid_orig] = meshgrid(freq.time, freq.freq);
[tim_grid_interp, freq_grid_interp] = meshgrid(tim_interp, freq_interp);

% And interpolate:
pow_interp = interp2(tim_grid_orig, freq_grid_orig, meanpow,...
    tim_grid_interp, freq_grid_interp, 'spline');
mask_interp = interp2(tim_grid_orig, freq_grid_orig, meanmask,...
    tim_grid_interp, freq_grid_interp, 'spline');

figure;
cfg = [];
cfg.mask = (abs(round(mask_interp)));
cfg.colormap = cmap_vis_ss;
call_js_private('mv_plot_2D', cfg, (pow_interp));
xticks([1 256 512])
xticklabels({num2str(freq.time(1)),'0', num2str(freq.time(end))});
yticks([1 256 512])
yticklabels({num2str(round(freq.freq(1))),'0', num2str(round(freq.freq(end)))});
set(gca, 'XTick', [], 'YTick', [])
caxis([-5 5]);
c = colorbar;
c.Ticks = [-5 0 5];
saveas(gcf, fullfile(fig_dir, 'gaze_stats_vis_ss.png'))

%% visual vs. multispeaker...
cfg = [];
cfg.avgoverchan = 'yes';
cfg.frequency = [-screen_rect_deg(4) screen_rect_deg(4)];
cfg.latency   = [-screen_rect_deg(3) screen_rect_deg(3)];
freq = ft_selectdata(cfg,stat_vis_vs_multi);
meanpow = squeeze(mean(freq.stat, 1));
meanmask = squeeze(mean(freq.mask, 1));
% The finer time and frequency axes:
tim_interp = linspace(freq.time(1), freq.time(end), 512);
freq_interp = linspace(-screen_rect_deg(4), screen_rect_deg(4), 512);
% mask_interp = linspace(-10, 10, 512);
% We need to make a full time/frequency grid of both the original and
% interpolated coordinates. Matlab's meshgrid() does this for us:
[tim_grid_orig, freq_grid_orig] = meshgrid(freq.time, freq.freq);
[tim_grid_interp, freq_grid_interp] = meshgrid(tim_interp, freq_interp);

% And interpolate:
pow_interp = interp2(tim_grid_orig, freq_grid_orig, meanpow,...
    tim_grid_interp, freq_grid_interp, 'spline');
mask_interp = interp2(tim_grid_orig, freq_grid_orig, meanmask,...
    tim_grid_interp, freq_grid_interp, 'spline');

figure;
cfg = [];
cfg.mask = (abs(round(mask_interp)));
cfg.colormap = cmap_vis_ms;
call_js_private('mv_plot_2D', cfg, (pow_interp));
xticks([1 256 512])
xticklabels({num2str(freq.time(1)),'0', num2str(freq.time(end))});
yticks([1 256 512])
yticklabels({num2str(round(freq.freq(1))),'0', num2str(round(freq.freq(end)))});
caxis([-5 5]);
c = colorbar;
c.Ticks = [-5 0 5];
saveas(gcf, fullfile(fig_dir, 'gaze_stats_vis_ms.svg'))